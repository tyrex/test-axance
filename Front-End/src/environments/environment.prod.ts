export const environment = {
  production: true,
  lang: ['en', 'fr'].includes(localStorage.getItem('lang')) ? localStorage.getItem('lang') : 'en',
  SERVER_URL: 'http://localhost:3000/',
  SERVER_RESOURCES_URL: 'http://localhost:3000/resources/',
  cached: false
};
