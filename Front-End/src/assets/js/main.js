$('.js-wp-1').waypoint((direction) => {
      $('.js-wp-1').addClass('animated fadeInDown');
    }, {
      offset: '50%'
    });


    $('.js-wp-2').waypoint((direction) => {
      $('.js-wp-2').addClass('animated fadeInLeft');
    }, {
      offset: '50%'
    });

    $('.js-wp-3').waypoint((direction) => {
      $('.js-wp-3').addClass('animated fadeInRight');
    }, {
      offset: '50%'
    });

    $('.js-wp-4').waypoint((direction) => {
      $('.js-wp-4').addClass('animated fadeInUp');
    }, {
      offset: '50%'
    });