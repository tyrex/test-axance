import {Injectable} from '@angular/core';
import {AUTH_ERRORS} from '../../configurations/AUTH_ERRORS';
import {STATUS_CONFIG} from '../../configurations/STATUS_CONFIG';
import {MODAL_MAP} from '../../configurations/modalMap';
import {ModalService} from '../modal/modal.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {
  public static passedErrFromBE = [];

  constructor() {
  }

  static handel(header, callBack?) {
    let errorCode = 'DEFAULT';
    if (header && header.statusCode) {
      errorCode = header.statusCode;
    }

    /**
     * Configuring the error message that will be shown to user
     */
    let errorMessage;
    if (AUTH_ERRORS[errorCode]) {
      errorMessage = AUTH_ERRORS[errorCode];
    } else {
      if (header.providerCode && header.providerCode === '04' && header.returnCodeDesc) {
        errorMessage = header.returnCodeDesc;
      } else {
        if (ErrorHandlerService.passedErrFromBE.includes(errorCode)) {
          errorMessage = header.titleEN;
        } else {
          errorMessage = AUTH_ERRORS.DEFAULT;
        }
      }
    }

    // const errorMessage = ErrorHandlerService.AUTH_ERRORS[errorCode] ? ErrorHandlerService.AUTH_ERRORS[errorCode] : (header.returnCodeDesc
    //   ? header.returnCodeDesc : (header.titleEN)); // ErrorHandlerService.AUTH_ERRORS['DEFAULT'];
    // TODO add modal service
    const data = {
      content: errorMessage,
      type: MODAL_MAP.ERROR.id
    };
    ModalService.modalEvent.emit(data);
    ModalService.showGlobalModal(MODAL_MAP.ERROR, errorMessage, callBack);

  }

}
