import {Injectable} from '@angular/core';

declare let $;

@Injectable({
  providedIn: 'root'
})
export class AnimationService {

  constructor() {
  }

  static animation1(className) {
    $('.' + className).waypoint((direction) => {
      $('.' + className).addClass('animated fadeInDown');
    }, {
      offset: '40%'
    });
  }

  static animation2(className) {
    $('.' + className).waypoint((direction) => {
      $('.' + className).addClass('animated fadeInUp');
    }, {
      offset: '40%'
    });
  }
}
