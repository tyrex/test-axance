import {EventEmitter} from '@angular/core';
import {MODAL_MAP} from '../../configurations/modalMap';

declare var $;


export class ModalService {
  // static modals: [any];
  static activeModal: string;

  public static modalEvent: EventEmitter<any> = new EventEmitter();

  public static cancelEvent: EventEmitter<any> = new EventEmitter();

  public static detailsEvent: EventEmitter<any> = new EventEmitter();

  public static mailOtpEvent: EventEmitter<any> = new EventEmitter();

  public static FormEvent: EventEmitter<any> = new EventEmitter();


  constructor() {
  }

  /**
   * @method showGlobalModal
   * @param modalObj
   * @param content
   * @param callBack
   */
  static showGlobalModal(modalObj: any, content: string, callBack?) {
    const errorData = {
      type: modalObj.type,
      id: modalObj.id,
      content,
      callBack: ModalService.getCallBackFunction(modalObj, callBack)
    };
    console.log('errorData ::', errorData);
    ModalService.modalEvent.emit(errorData);
    ModalService.show(modalObj.id);
  }

  /**
   * @method showCancelModal
   * @param callBack
   * @param modalData
   */
  static showCancelModal(modalData: any, callBack?) {
    const data = {
      content: 'COMMON.GO_BACK_CONFIRM_MODAL_CONTENT', // modalData.modalContent,
      cancelCallBack() {
        ModalService.hide(MODAL_MAP.CANCEL.id);
      },
      callBack: ModalService.getCallBackFunction(MODAL_MAP.CANCEL, callBack)
    };

    ModalService.cancelEvent.emit(data);
    ModalService.show(MODAL_MAP.CANCEL.id);

  }


  static showDetailsModal(modalData: any, callBack?) {
    const data = {
      content: 'SERVICES.REQUEST_DETAILS', // modalData.modalContent
      request: modalData.request,
      callBack: ModalService.getCallBackFunction(MODAL_MAP.DETAILS, callBack)
    };
    ModalService.detailsEvent.emit(data);
    ModalService.show(MODAL_MAP.DETAILS.id);
  }

  static confirmModal(modalData: any, callBack?) {
    const data = {
      content: modalData.content, // modalData.modalContent
      ref: modalData.id,
      type: modalData.type,
      request: modalData.request,
      callBack: ModalService.getCallBackFunction(modalData, callBack)
    };
    console.log('data ::', data);
    ModalService.FormEvent.emit(data);
    ModalService.show(modalData.id);
  }

  static showConfirmationModal(modalData: any, content: string, service?: any, item?: any, callBack?) {
    const data = {
      modalType: modalData.type,
      modalRef: modalData.id,
      service,
      content,
      request: item,
      callBack: ModalService.getCallBackFunction(modalData, callBack)
    };
    console.log('data ::', data);
    ModalService.detailsEvent.emit(data);
    ModalService.show(modalData.id);
  }

  static showFormsModal(modalData: any, content: string, item: any, callBack?) {
    const data = {
      type: modalData.type,
      ref: modalData.id,
      content: content,
      request: item,
      callBack: ModalService.getCallBackFunction(modalData, callBack)
    };
    console.log('data ::', data);
    ModalService.FormEvent.emit(data);
    ModalService.show(modalData.id);
  }


  static show(id: string) {
    if (ModalService.activeModal) {
      ModalService.hide(ModalService.activeModal);
    }
    ModalService.activeModal = id;
    $('#modal-' + id).modal('show');
  }

  static hide(id: string) {
    $('#modal-' + id).modal('hide');
  }

  static hideAll() {
    $('#modal-').modal('hide');
  }

  /**
   * @method getCallBackFunction
   * @param callBack
   * @param modalObj
   * @returns {()=>undefined}
   */
  private static getCallBackFunction(modalObj, callBack): any {
    // let globalCallBack;
    if (typeof callBack === 'undefined') {
      return () => {
        ModalService.hide(modalObj.id);
      };
    } else {
      return () => {
        callBack();
        ModalService.hide(modalObj.id);
      };
    }
  }

}
