import {Injectable} from '@angular/core';
import {AbstractControl, FormControl, ValidatorFn, Validators} from '@angular/forms';
import {isArabicNumeric, isNumeric, isPresent, validPhone} from '../../../core/utils/utility-functions';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor() {
  }

  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    const config = {
      required: 'FORM_ERROR_MESSAGES.REQUIRED',
      invalidCreditCard: 'FORM_ERROR_MESSAGES.INVALID_CREDIT_CARD',
      invalidEmailAddress: 'FORM_ERROR_MESSAGES.INVALID_EMAIL_ADDRESS',
      email: 'FORM_ERROR_MESSAGES.INVALID_EMAIL_ADDRESS',
      invalidNickName: 'FORM_ERROR_MESSAGES.INVALID_NICKNAME',
      emailNotmatch: 'FORM_ERROR_MESSAGES.EMAIL_NOT_MATCH',
      invalidPassword: 'FORM_ERROR_MESSAGES.INVALID_PASSWORD',
      passwordNotmatch: 'FORM_ERROR_MESSAGES.PASSWORD_NOT_MATCH',
      pinNotmatch: 'FORM_ERROR_MESSAGES.PIN_NOT_MATCH',
      minlength: {text: 'FORM_ERROR_MESSAGES.MIN_LENGTH', param: '' + validatorValue},
      maxlength: {text: 'FORM_ERROR_MESSAGES.MAX_LENGTH', param: '' + validatorValue},
      maxNumber: {text: 'FORM_ERROR_MESSAGES.MAX_VALUE', param: '' + validatorValue.max},
      minNumber: {text: 'FORM_ERROR_MESSAGES.MIN_VALUE', param: '' + validatorValue.min},
      minMaxNumber: {
        text: 'FORM_ERROR_MESSAGES.MIN_MAX_VALUE',
        param: `(${validatorValue.min}) - (${validatorValue.max})`,
      },
      invalidNumber: 'FORM_ERROR_MESSAGES.INVALID_NUMBER',
      invalidCardNumber: 'FORM_ERROR_MESSAGES.INVALID_CARD_NUMBER',
      invalidPinNumber: 'FORM_ERROR_MESSAGES.INVALID_PIN_NUMBER',
      invalidPinNumber1: 'FORM_ERROR_MESSAGES.INVALID_PIN_NUMBER1',
      invalidBioNumber: 'FORM_ERROR_MESSAGES.INVALID_BIO_NUMBER',
      shouldContainOneDigits: 'FORM_ERROR_MESSAGES.SHOULD_CONTAIN_ONE_DIGITS',
      shouldContainFourDigits: 'FORM_ERROR_MESSAGES.SHOULD_CONTAIN_FOUR_DIGITS',
      dateNow: 'FORM_ERROR_MESSAGES.DATE_NOW',
      swiftCodeMissmatch: 'FORM_ERROR_MESSAGES.SWIFT_CODE_MISSMATCH',
      minStartDate: 'FORM_ERROR_MESSAGES.MIN_START_DATE',
      minEndDate: 'FORM_ERROR_MESSAGES.MIN_END_DATE',
      notValidDate: 'FORM_ERROR_MESSAGES.NOT_VALID_DATE',
      maxAmountShouldNotExeedBalance: 'FORM_ERROR_MESSAGES.TRANSFER_AMOUNT_SHOULD_NOT_EXCEED_AVAILABLE_BALANCE',
      Prefix_Invalid_Pattern: 'FORM_ERROR_MESSAGES.PREFIX_INVALID_PATTERN',
      Eleven_Digit_Invalid_Pattern: 'FORM_ERROR_MESSAGES.ELEVEN_DIGIT_INVALID_PATTERN',
      Only_Numbers_Invalid_Pattern: 'FORM_ERROR_MESSAGES.ONLY_NUMBERS_INVALID_PATTERN',
      minAmount: {
        text: 'FORM_ERROR_MESSAGES.MIN_AMOUNT',
        param: validatorValue
      },
      maxAmount: {
        text: 'FORM_ERROR_MESSAGES.MAX_AMOUNT',
        param: validatorValue
      },
      invalidPass: {
        text: 'FORM_ERROR_MESSAGES.INVALID_PASS',
        param: validatorValue
      },
      different: {
        text: 'FORM_ERROR_MESSAGES.DIFFERENT',
        param: validatorValue
      },
      invalidMinFiles: {
        text: 'FORM_ERROR_MESSAGES.MIN_FILES',
        param: validatorValue
      },
      invalidMaxFiles: {
        text: 'FORM_ERROR_MESSAGES.MAX_FILES',
        param: validatorValue
      },
      min: 'FORM_ERROR_MESSAGES.AMOUNT_INVALID',
      minNombre: 'FORM_ERROR_MESSAGES.NUMBER_INVALID',
      nonAlphanumericUserName: 'FORM_ERROR_MESSAGES.NON_ALPHANUMERIC_USERNAME',
      pattern: 'FORM_ERROR_MESSAGES.PATTERN_INVALID',
      INVALID_AMOUNT: 'CARDS.INVALID_AMOUNT',
      INVALID_ACOUNT: 'CARDS.PLEASE_SELECT_AN_ACCOUNT',
      minDate: {
        text: 'FORM_ERROR_MESSAGES.MIN_DATE',
        param: validatorValue
      },
      maxDate: {
        text: 'FORM_ERROR_MESSAGES.MAX_DATE',
        param: validatorValue
      },
      invalidRIB: {
        text: 'FORM_ERROR_MESSAGES.INVALID_RIB',
        param: validatorValue
      },
      invalidTel: {
        text: 'FORM_ERROR_MESSAGES.INVALID_TEL',
        param: validatorValue
      },
      REGEX: 'FORM_ERROR_MESSAGES.REGEX',
      INVALID_USERNAME: {
        text: 'FORM_ERROR_MESSAGES.INVALID_USERNAME',
        param: validatorValue
      },
      INVALID_FORMAT: {
        text: 'FORM_ERROR_MESSAGES.INVALID_FORMAT',
        param: validatorValue
      },
      INVALID_LENGTH: {
        text: 'FORM_ERROR_MESSAGES.INVALID_LENGTH',
        param: validatorValue
      }
    };

    return config[validatorName];
  }

  //  >> ValidationService.number({max: 20})
  static number(prms: any = {}): ValidatorFn {
    return (control: FormControl): { [key: string]: any } => {
      if (isPresent(Validators.required(control))) {
        return null;
      }
      const val = control.value;
      if (!isNumeric(val)) {
        return {invalidNumber: true};
      } else if (!isNaN(prms.min) && !isNaN(prms.max)) {

        return val < prms.min || val > prms.max ? {minMaxNumber: {max: prms.max, min: prms.min}} : null;
      } else if (!isNaN(prms.min)) {

        return val < prms.min ? {minNumber: {min: prms.min}} : null;
      } else if (!isNaN(prms.max)) {

        return val > prms.max ? {maxNumber: {max: prms.max}} : null;
      } else {
        return null;
      }
    };
  }

  static onlyNumbers(): ValidatorFn {
    return (control: FormControl): { [key: string]: any } => {
      const val = control.value;
      console.log(val);
      if (!isNumeric(val) && !isArabicNumeric(val)) {
        return {invalidNumber: true};
      } else {
        return null;
      }
    };
  }

  /**
   * @description validates a form control min with a timestamp
   */
  static minDateValidator(min: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value !== undefined && (!new Date(control.value) || moment(control.value).valueOf() < moment(min).valueOf())) {
        return {minDate: moment(new Date(min).getTime()).format('DD/MM/YYYY')};
      }
      return null;
    };
  }

  /**
   * @description validates a form control max with a timestamp
   */
  static maxDateValidator(max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value !== undefined && (!new Date(control.value) || moment(control.value).valueOf() > moment(max).valueOf())) {
        return {maxDate: moment(new Date(max).getTime()).format('DD/MM/YYYY hh:mm')};
      }
      return null;
    };
  }

  static minAmountValidator(min: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value) {
        if ((typeof control.value === 'number' && control.value < min)
          || (typeof control.value === 'string' && parseFloat(control.value) < min)) {
          return {minAmount: min};
        }
      }
      return null;
    };
  }

  static maxAmountValidator(max: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value) {
        if ((typeof control.value === 'number' && control.value > max)
          || (typeof control.value === 'string' && parseFloat(control.value) > max)) {
          return {maxAmount: max};
        }
      }
      return null;
    };
  }

  static passwordValidator(): ValidatorFn {
    console.log('passwordValidator');
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value) {
        console.log(control.value);
        if (!RegExp('[A-Z]').test(control.value)) {
          return {invalidPass: 'doit contenir au moins une lettre majuscule'};
        } else if (!RegExp('[a-z]').test(control.value)) {
          return {invalidPass: 'doit contenir au moins une lettre miniscule'};
        } else if (!RegExp('[0-9]').test(control.value)) {
          return {invalidPass: 'doit contenir au moins un chiffre'};
        } else if (!RegExp('[!@#$%^&*()-+/;§µ£°~`,.?":{}|<>]').test(control.value)) {
          return {invalidPass: 'doit contenir au moins un charactére spécial'};
        } else if (control.value.length < 8) {
          return {invalidPass: 'doit contenir au moins 8 charactére'};
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }

  static minFiles(minFiles: any, description: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && control.value.length < minFiles) {
        console.log(control.value);
        console.log(description);
        return {invalidMinFiles: description};
      } else {
        return null;
      }
    };
  }

  static maxFiles(maxFiles: any, description: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && control.value.length > maxFiles) {
        console.log(control.value);
        return {invalidMaxFiles: description};
      } else {
        return null;
      }
    };
  }

  static ribValidator(): ValidatorFn {
    console.log('ribValidator');
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value) {
        console.log(control.value);
        if (!RegExp('^[0-9]{20}$').test(control.value)) {
          return {invalidRIB: 'Doit être composé de 20 chiffres'};
        }
      } else {
        return null;
      }
    };
  }

  static telValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value !== null) {
        console.warn(control.value.toString());
        if (control.value.toString().substr(0, 1) !== '+') {
          return {Prefix_Invalid_Pattern: ''};
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }

  static minNumberLength(minLength): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value) {
        if (control.value.toString().length < Number(minLength)) {
          return {minlength: minLength};
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }

  static maxNumberLength(maxLength): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value) {
        if (control.value.toString().length > Number(maxLength)) {
          return {maxlength: maxLength};
        } else {
          return null;
        }
      } else {
        return null;
      }
    };
  }

  static differentValidator(toCompareWith: string, propertyName: string): ValidatorFn {
    console.log(toCompareWith);
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && toCompareWith && control.value.toLowerCase().indexOf(toCompareWith.toLowerCase()) !== -1) {
        return {different: propertyName};
      }
      return null;
    };
  }

  static regexValidator(pattern: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && !RegExp(pattern).test(control.value.toString())) {
        console.log(pattern, control.value);
        return {REGEX: true};
      }
      return null;
    };
  }

  static userNameValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && RegExp('[!@#$%^&*()-+/\';§µ£°~`,.?":{}|<>]').test(control.value.toString())) {
        console.log(control.value);
        return {INVALID_USERNAME: 'Doit pas contenir un caractére spécial'};
      } else if (control.value && (control.value.length > 20 || control.value.length < 4)) {
        return {INVALID_USERNAME: 'Doit être entre 4 et 20 caractéres'};
      }
      return null;
    };
  }

  static onlyChars(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && RegExp('[0-9]').test(control.value.toString())) {
        console.log(control.value);
        return {INVALID_FORMAT: 'Doit pas contenir un chiffre'};
      }
      return null;
    };
  }

  // static exactLength(length: number): ValidatorFn {
  //   return (control: AbstractControl): { [key: string]: any } | null => {
  //     if (control.value.toString().length !== length) {
  //       console.log(control.value);
  //       return {'INVALID_LENGTH': 'Doit  contenir ' + length + ' chiffres'};
  //     }
  //     return null;
  //   };
  // }

  static matchingValidator(toCompareWith: string): ValidatorFn {
    console.log(toCompareWith);
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (control.value && toCompareWith && control.value.toLowerCase() !== toCompareWith.toLowerCase()) {
        return {INVALID_USERNAME: 'Le nom d\'utilisateur est incorrect'};
      }
      return null;
    };
  }
}
