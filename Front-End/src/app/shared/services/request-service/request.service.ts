import {Injectable} from '@angular/core';
import {LoaderService} from '../loader-service/loader.service';
import {isBoolean} from 'util';
import {environment} from '../../../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {REQUEST_METHODS} from '../../configurations/REQUEST_METHODS';
import {catchError, retry} from 'rxjs/internal/operators';
import {Observable, of} from 'rxjs';
import {ErrorHandlerService} from '../error-handler/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  static results;

  constructor(private http: HttpClient) {
  }

  static log(message: string) {
    console.log(message);
  }


  /**
   * test if without loader
   * @param extras
   */
  static withoutLoader(extras) {
    return extras && isBoolean(extras.showLoader) && !extras.showLoader;
  }

  /**
   * Test if without cache
   * @param extras
   */
  static withoutCache(extras) {
    if (extras && isBoolean(extras.cached) && !extras.cached) {
      return true;
    } else {
      return isBoolean(environment.cached) && !environment.cached;
    }
  }

  sendRequest({resources, params}: { resources: any, params: any },
              extras?: { showLoader?: boolean, cached?: boolean }): Promise<any> {
    /**
     * If the loader is required
     */
    if (!RequestService.withoutLoader(extras)) {
      LoaderService.showLoaderOn();
    }

    /**
     * Insitializing request params
     */
    const self = {
      path: environment.SERVER_URL + resources.path,
      method: resources.method,
      requestTimeout: resources.requestTimeout ? resources.requestTimeout : 1000 // TODO default value from config
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'jwt-token'
      })
    };

    let requestToInvoke: Promise<any>;

    // -------------------------------------- Invoke Get Request -------------------------------------
    if (self.method === REQUEST_METHODS.GET) {
      // Http Options containing headers and query params
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'jwt-token'
        }),
        params: new HttpParams({fromObject: params})
      };

      requestToInvoke = this.http.get<any>(self.path, httpOptions).pipe(
        retry(3), catchError(this.handleError<any>(self.path, []))).toPromise();
      // ------------------------------------ End Invoke Get Request -------------------------------------

      // -------------------------------------- Invoke POST Request -------------------------------------
    } else if (self.method === REQUEST_METHODS.POST) {
      requestToInvoke = this.http.post<any>(self.path, params, httpOptions).pipe(
        retry(3), catchError(this.handleError<any>(self.path, []))).toPromise();
      // ------------------------------------ End Invoke Get Request -------------------------------------

      // -------------------------------------- Invoke PUT Request -------------------------------------
    } else if (self.method === REQUEST_METHODS.PUT) {
      requestToInvoke = this.http.put<any>(self.path, params, httpOptions).pipe(
        retry(3), catchError(this.handleError<any>(self.path, []))).toPromise();
      // ------------------------------------ End Invoke PUT Request -------------------------------------
    }

    return new Promise((resolve, reject) => {
      requestToInvoke.then((result) => {
        /**
         * If the loader is required
         */
        if (!RequestService.withoutLoader(extras)) {
          LoaderService.hideLoaderFrom();
        }

        /**
         * If the caching system is required
         */
        if (!RequestService.withoutCache(extras)) {
          RequestService.results[self.path] = result;
        }
        console.log('request service ----->', result);
        if (result.messageStatus.returnCode === '000') {
          resolve(result.messageBody);
        } else {
          /**
           * Centralized error handling
           */
          ErrorHandlerService.handel(result.messageStatus);
          reject(result);
        }
      }, (error) => {
        if (!RequestService.withoutLoader(extras)) {
          LoaderService.hideLoaderFrom();
        }
      }).catch((error) => {
        if (!RequestService.withoutLoader(extras)) {
          LoaderService.hideLoaderFrom();
        }

        /**
         * Centralized error handling
         */
        ErrorHandlerService.handel(error.messageBody);
        reject(error);
      });
    });

  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      RequestService.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

}
