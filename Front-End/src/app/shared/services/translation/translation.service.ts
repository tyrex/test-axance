import {ElementRef, Injectable, NgZone} from '@angular/core';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {


  constructor(private translateService: TranslateService,
              private ngZone: NgZone
  ) {
  }

  initTranslationLanguage() {
    this.translateService.setDefaultLang(environment.lang);
    this.translateService.use(environment.lang);
    this.updateLanguage(environment.lang);
  }

  setLanguage(lang: string) {
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
    this.updateLanguage(lang);
  }

  /**
   * Update the language in the lang attribute of the html element.
   */
  updateLanguage(language: string): void {
    localStorage.setItem('lang', language);
    environment.lang = language;
  }


}






