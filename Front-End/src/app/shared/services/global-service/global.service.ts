import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  static bgClasses = ['bg-blue', 'bg-white-blue', 'bg-black', 'bg-white-grey', 'bg-red-blue'];

  constructor() {
  }

  // ******************************** Get random background for cards *********************************//
  static getRandomBgClass(index) {
    return GlobalService.bgClasses[index % GlobalService.bgClasses.length];
  }
  // ------------------------------------------- End ----------------------------------------------
}
