import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  static activeRequestNumber = 0;
  private loaderSubject = new BehaviorSubject<any>({show: false});

  constructor() {
  }

  /**
   * @description shows a loader spinner on element based on id passed in params
   */
  static showLoaderOn(elementId?: string) {
    this.activeRequestNumber++;
    if (this.activeRequestNumber === 1) {
      const loaderParent = document.createElement('div');
      loaderParent.id = 'loader-wrapper';
      const loader = document.createElement('i');

      loaderParent.className = 'd-flex align-items-center justify-content-center';
      loader.className = 'action-loader';

      loader.style.fontSize = '48px';
      loader.style.color = 'blue';
      loader.id = 'loader-' + (elementId || 'wrapper-outlet');
      document.getElementById((elementId || 'wrapper-outlet'))
        .appendChild(loaderParent).appendChild(loader);
    }
  }

  /**
   * @description hides the loader spinner from element based on id passed in params
   */
  static hideLoaderFrom(elementId?: string) {
    this.activeRequestNumber--;
    if (this.activeRequestNumber <= 0) {
      this.activeRequestNumber = 0;
      if (document.getElementById('loader-' + (elementId || 'wrapper-outlet'))) {
        document.getElementById(elementId || 'wrapper-outlet')
          .removeChild(document.getElementById('loader-wrapper'));
      }
    }

  }

  /**
   * @description hides all the loaders spinners from element based on id passed in params
   */
  static hideAllLoaderFrom(elementId?: string) {
    if (this.activeRequestNumber > 0) {
      this.activeRequestNumber = 0;
      if (document.getElementById('loader-' + (elementId || 'wrapper-outlet'))) {
        document.getElementById(elementId || 'wrapper-outlet')
          .removeChild(document.getElementById('loader-wrapper'));
      }
    }

  }

  /**
   * show the loader
   */
  show() {
    this.loaderSubject.next({show: true});
  }

  /**
   * hide the loader
   */
  hide() {
    this.loaderSubject.next({show: false});
  }
}
