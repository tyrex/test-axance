import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'safeHtml'
})
export class SafeHtmlPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(value: any, type: any): any {

    if (type === 'html') {
      return this.sanitizer.bypassSecurityTrustHtml(value);
    } else if (type === 'imgSrc') {
      return this.sanitizer.bypassSecurityTrustResourceUrl(value);
    }
  }

}
