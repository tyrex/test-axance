import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {TranslateDirective, TranslateModule} from '@ngx-translate/core';
import {ImageSrcPipe} from './pipes/image-src/image-src.pipe';
import {SafeHtmlPipe} from './pipes/safe-html/safe-html.pipe';


@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    TranslateModule
  ],
  declarations: [
    ImageSrcPipe,
    SafeHtmlPipe
  ],
  exports: [
    TranslateModule,
    HttpClientModule,
    ImageSrcPipe,
    SafeHtmlPipe,
    TranslateDirective
  ],
  providers: []
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule, providers: [
        TranslateDirective
      ]
    };
  }
}
