export const STRINGS = {
  LANG: 'lang',
  ENVIRONMENT: 'environment',
  EN: 'en',
  AR: 'ar',
  ADMINS: 'ADMINS',
  SERVICES: 'SERVICES'
};

