export const MODAL_MAP: any = {
  ERROR: {id: 'error', type: 'error'},
  CONFIRM: {id: 'confirmModal', type: 'confirm'},
  CONFIRM_DETAILS: {id: 'confirm_details', type: 'confirm'},
  CONFIRM_DETAIL: {id: 'confirm_details', type: 'info'},
  CONFIRM_FORM: {id: 'confirm_form', type: 'form'},
  CONFIRM_PROROGATION: {id: 'confirm_prorogation', type: 'form'},
  INFO: {id: 'confirmModal', type: 'info'},
  DETAILS:  {id: 'details', type: 'info'},
  PUSH:  {id: 'push', type: 'info'},
  CANCEL: {id: 'cancel_modal', type: 'confirm'},
  SUCCESS: {id: 'success', type: 'info'},
};
