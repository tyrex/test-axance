import {Catalog} from "../../models/Catalog";

export default [
  {
    "id": 1,
    "name": "Music"
  },
  {
    "id": 2,
    "name": "Beauty"
  },
  {
    "id": 3,
    "name": "Movies"
  },
  {
    "id": 4,
    "name": "Books"
  },
  {
    "id": 5,
    "name": "Health"
  },
  {
    "id": 6,
    "name": "Outdoors"
  },
  {
    "id": 7,
    "name": "Computers"
  },
  {
    "id": 8,
    "name": "Computers"
  },
  {
    "id": 9,
    "name": "Sports"
  }
] as Catalog[];
