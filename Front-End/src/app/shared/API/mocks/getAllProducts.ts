import {Product} from "../../models/Product";

export default [
  {
    "id": 1,
    "productName": "Handcrafted Cotton Hat",
    "price": "624.00",
    "author": "John Doelson",
    "img": "products/product1.png",
    "productDescription": "Ea blanditiis distinctio amet unde rerum provident. Dolores deserunt aut. Debitis sunt ducimus quisquam non voluptas dolor rem. Autem ut nemo.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 4,
      "name": "Books"
    }
  },
  {
    "id": 4,
    "productName": "Gorgeous Fresh Ball",
    "price": "685.00",
    "author": "John Doelson",
    "img": "products/product5.png",
    "productDescription": "Suscipit autem qui odit dolorum. Hic excepturi voluptatem enim repellendus voluptas officiis. Dolores iste voluptas labore sit. Vitae distinctio consequatur et fuga aut error.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 2,
      "name": "Beauty"
    }
  },
  {
    "id": 5,
    "productName": "Small Fresh Chicken",
    "price": "879.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Corporis corporis distinctio aut ducimus ab voluptates amet deleniti consequuntur. Ipsum illum nisi est suscipit est. Enim ipsa eveniet expedita architecto quia corporis nihil temporibus aut. Dolorem placeat eaque quod qui deserunt est amet fugiat.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 4,
      "name": "Books"
    }
  },
  {
    "id": 7,
    "productName": "Refined Metal Chips",
    "price": "931.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Praesentium error vel esse vero. Quibusdam eius reprehenderit nostrum quae. Dolorum occaecati esse molestiae neque quibusdam.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 5,
      "name": "Health"
    }
  },
  {
    "id": 8,
    "productName": "Sleek Wooden Pizza",
    "price": "475.00",
    "author": "John Doelson",
    "img": "products/product5.png",
    "productDescription": "Quisquam pariatur vel ad asperiores vel odio dolorem eos optio. Qui ducimus ullam. Nobis necessitatibus nemo ad dolores. Quis nostrum facilis temporibus asperiores facere. Ut ea reiciendis molestias ab sapiente iusto modi veritatis ab.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 5,
      "name": "Health"
    }
  },
  {
    "id": 13,
    "productName": "Awesome Granite Shirt",
    "price": "1.00",
    "author": "John Doelson",
    "img": "products/product5.png",
    "productDescription": "Nostrum sed ab expedita est. Dolorem facilis quod optio et atque cumque. Libero voluptatem sed reprehenderit veritatis sed dolorem.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 1,
      "name": "Music"
    }
  },
  {
    "id": 14,
    "productName": "Unbranded Steel Keyboard",
    "price": "304.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Ea nam at hic recusandae nihil commodi qui ut aut. Perferendis quas sit ipsum omnis sapiente aut. Molestias laudantium impedit velit aut. Ut dolores harum.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 3,
      "name": "Movies"
    }
  },
  {
    "id": 16,
    "productName": "Generic Frozen Shoes",
    "price": "335.00",
    "author": "John Doelson",
    "img": "products/product3.png",
    "productDescription": "Ut sed fugiat voluptatem consequatur adipisci veritatis. Tenetur nam voluptas minima. Est et commodi et. Iusto quae sunt vero voluptatum officia velit.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 7,
      "name": "Computers"
    }
  },
  {
    "id": 17,
    "productName": "Licensed Cotton Pizza",
    "price": "116.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Ut nisi quis quo dolorem voluptas nemo. Atque est tempore. Molestiae repellendus quasi non harum nesciunt ipsam quasi sint.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 4,
      "name": "Books"
    }
  },
  {
    "id": 19,
    "productName": "Refined Soft Table",
    "price": "797.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Non atque in libero temporibus non vero possimus delectus sit. Magni beatae quia magnam a dolorem. Incidunt voluptas cupiditate facilis enim ut.",
    "Category": {
      "id": 1,
      "name": "INDOORS"
    },
    "Catalog": {
      "id": 4,
      "name": "Books"
    }
  },
  {
    "id": 2,
    "productName": "Small Concrete Cheese",
    "price": "472.00",
    "author": "John Doelson",
    "img": "products/product5.png",
    "productDescription": "Esse nobis eaque laboriosam consectetur qui et rem est ducimus. Reiciendis repellat inventore quibusdam aliquid. Facilis minima aut aut nostrum voluptatibus. Id id sed sed. Excepturi ipsam voluptates excepturi eos ipsum.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 1,
      "name": "Music"
    }
  },
  {
    "id": 3,
    "productName": "Awesome Frozen Chips",
    "price": "839.00",
    "author": "John Doelson",
    "img": "products/product1.png",
    "productDescription": "Doloremque quae et et necessitatibus iusto consequatur aut distinctio. Aliquid provident sed praesentium. Porro aut animi ut eveniet aut.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 8,
      "name": "Computers"
    }
  },
  {
    "id": 6,
    "productName": "Refined Rubber Bacon",
    "price": "690.00",
    "author": "John Doelson",
    "img": "products/product2.png",
    "productDescription": "Omnis numquam qui quae enim. Nobis repellendus eos sit dicta qui. Consequatur molestiae quia omnis maxime omnis eveniet praesentium ut. Voluptas placeat culpa vel recusandae non sit illo et. Et esse quaerat eius temporibus.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 2,
      "name": "Beauty"
    }
  },
  {
    "id": 9,
    "productName": "Refined Plastic Shoes",
    "price": "354.00",
    "author": "John Doelson",
    "img": "products/product2.png",
    "productDescription": "Qui numquam blanditiis quaerat repellendus voluptas soluta. Quam excepturi ad et consectetur sed voluptatibus. Aut voluptate commodi aut qui rem culpa aliquid et voluptates.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 3,
      "name": "Movies"
    }
  },
  {
    "id": 10,
    "productName": "Fantastic Frozen Gloves",
    "price": "354.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Consequatur eos ea aut minima. Dicta ut omnis. Blanditiis dolorem veniam reprehenderit iusto excepturi quo commodi eum adipisci. Et voluptas accusantium nobis quibusdam aut.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 8,
      "name": "Computers"
    }
  },
  {
    "id": 11,
    "productName": "Incredible Cotton Hat",
    "price": "305.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Sapiente et maxime corrupti provident adipisci deleniti dolorem nisi. A ut fuga ut. Qui esse ullam doloremque minus sint voluptatibus a deserunt. Nihil impedit eligendi culpa et est enim optio et.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 9,
      "name": "Sports"
    }
  },
  {
    "id": 12,
    "productName": "Tasty Granite Ball",
    "price": "458.00",
    "author": "John Doelson",
    "img": "products/product5.png",
    "productDescription": "Aliquid vel et voluptas. Voluptatum qui esse voluptatem et ut mollitia aut in ex. Eveniet animi ut odio ut animi suscipit doloremque.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 9,
      "name": "Sports"
    }
  },
  {
    "id": 15,
    "productName": "Incredible Concrete Chicken",
    "price": "115.00",
    "author": "John Doelson",
    "img": "products/product4.png",
    "productDescription": "Ratione dolor exercitationem unde eligendi eum alias et et. Est et quaerat cum similique tempore. Natus placeat velit tempore dolor ea earum dolorum consequuntur iste. Qui repudiandae necessitatibus. Doloremque autem earum consequuntur similique perferendis et. Occaecati consectetur rem temporibus consequuntur accusamus.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 7,
      "name": "Computers"
    }
  },
  {
    "id": 18,
    "productName": "Tasty Granite Car",
    "price": "653.00",
    "author": "John Doelson",
    "img": "products/product1.png",
    "productDescription": "Occaecati impedit aut dignissimos blanditiis. Necessitatibus eius esse beatae quibusdam ipsam reiciendis. Doloremque hic nam temporibus ut. Et beatae aut ea aut facere necessitatibus.",
    "Category": {
      "id": 2,
      "name": "OUTDOORS"
    },
    "Catalog": {
      "id": 2,
      "name": "Beauty"
    }
  }
] as Product[];
