export const APIS: any = {
  PRODUCTS: {
    GetProductList: {
      path: 'products/getAllProducts',
      method: 'GET',
      requestTimeout: 2000
    },
  },
  CATALOGS: {
    GetCatalogList: {
      path: 'products/getAllCatalogs',
      method: 'GET',
      requestTimeout: 2000
    }
  }
};
