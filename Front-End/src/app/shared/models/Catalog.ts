export interface Catalog {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}
