export interface Product {
  id: number;
  productName: string;
  price: string;
  productDescription: string;
  author: string;
  img: string;
  Catalog: any;
  Category: any;
  createdAt: Date;
  updatedAt: Date;
}
