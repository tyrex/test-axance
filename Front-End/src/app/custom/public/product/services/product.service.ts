import {Injectable} from '@angular/core';
import {RequestService} from '../../../../shared/services/request-service/request.service';
import {APIS} from '../../../../shared/API/API';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private requestService: RequestService) {
  }

  getAllProducts(params?) {
    return this.requestService.sendRequest({resources: APIS.PRODUCTS.GetProductList, params});
  }

  getAllCatalogs() {
    return this.requestService.sendRequest({resources: APIS.CATALOGS.GetCatalogList, params: {}});
  }
}
