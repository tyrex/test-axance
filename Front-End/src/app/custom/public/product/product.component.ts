import {Component, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../../../shared/models/Product';
import {ProductService} from './services/product.service';
import {getTotalPages, nextPage, previousPage} from '../../../core/utils/utility-functions';
import {ActivatedRoute} from '@angular/router';
import {MODAL_MAP} from '../../../shared/configurations/modalMap';
import {ModalService} from '../../../shared/services/modal/modal.service';
import {Catalog} from '../../../shared/models/Catalog';
import {Subscription} from 'rxjs';
import getAllProducts from "../../../shared/API/mocks/getAllProducts";
import getAllCatalogs from "../../../shared/API/mocks/getAllCatalogs";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {

  products: Product[] = [];
  filteredProducts: Product[] = [];
  productsToShow: Product[] = [];

  catalogs: Catalog[] = [];

  // Filters got from catalogs list
  filters: any[] = [];

  // Query param subscription
  sub: Subscription;
  keyword: string;

  sectionSettings = {index: 0, itemsPerPage: 8};

  constructor(private productService: ProductService,
              private route: ActivatedRoute) {
  }


  ngOnInit() {
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to '' if no query param provided.
        this.keyword = params.keyword || '';
        this.getAllProducts();
      });
    this.getAllCatalogs();
  }


  goToPrevious() {
    this.productsToShow = previousPage(this.filteredProducts, this.productsToShow, this.sectionSettings);
  }

  goToNext() {
    this.productsToShow = nextPage(this.filteredProducts, this.productsToShow, this.sectionSettings);
  }

  getTotalPages() {
    return getTotalPages(this.filteredProducts.length, this.sectionSettings.itemsPerPage);
  }


  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  /**
   *
   *
   *
   *
   *
   */
  // ************************************** Get all products ********************************//
  getAllProducts() {
    this.productService.getAllProducts({search: this.keyword}).then((result: Product[]) => {
      this.products = result;
      this.initializeProducts();
    }, (error) => {
      console.error(error);
      this.products = getAllProducts;
      this.initializeProducts();
    });
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ***************************************** Get all catalogs ************************************//
  getAllCatalogs() {
    this.productService.getAllCatalogs().then((result: Catalog[]) => {
      this.catalogs = result;
    }, (error) => {
      console.error(error);
      this.catalogs = getAllCatalogs;
    });
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ********************************** When user remove filter from UI  *******************************//
  removeFilter(catalog: Catalog) {
    // Resetting Pagination to 0
    this.sectionSettings.index = 0;

    // Revert back filter to catalogs array to show it in the list of filters can be selected
    this.filters = this.filters.filter((item: Catalog, index) => {
      return item.id !== catalog.id;
    });

    // Push back the catalog to the catalogs array, as to be presented in the filter list in the modal
    this.catalogs.push(catalog);

    // Getting all products if there is no filters
    if (this.filters.length === 0) {
      this.initializeProducts();
      return;
    }

    // Filtering products which they have catalog id equal to filter id
    this.filterProducts();
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ********************************** When user add filter from UI  *******************************//
  addFilter(catalog) {
    // Resetting Pagination to 0
    this.sectionSettings.index = 0;

    // Push catalog chosen to filter array
    this.filters.push(catalog);

    // Get out the filter from catalogs array
    this.catalogs = this.catalogs.filter((item, index) => {
      return item.id !== catalog.id;
    });

    // Filtering products which they have catalog id equal to filter id
    this.filterProducts();
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ******************************* Filtering products by filters array  *****************************//
  filterProducts() {
    // Get Filters IDs
    const catalogsID = this.filters.map(item => item.id);

    // Filtering products which they have catalog id in filter IDs array
    this.filteredProducts = this.products.filter((item, index) => {
      return catalogsID.includes(item.Catalog.id);
    });

    // Pagination
    this.productsToShow = this.filteredProducts.slice(0, this.sectionSettings.itemsPerPage);
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ******************************* Initialize products from origin array  *****************************//
  initializeProducts() {
    this.filteredProducts = this.products;
    this.productsToShow = this.products.slice(0, this.sectionSettings.itemsPerPage);
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ******************************* Open filters popup  *****************************//
  openFilterPopup() {
    ModalService.show(MODAL_MAP.INFO.type);
  } // ------------------------------------------- End ----------------------------------------------


}
