import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProductComponent} from './product.component';
import {ProductRoutingModule} from './product-routing.module';
import {LayoutModule} from '../../layout/layout.module';
import {SharedModule} from '../../../shared/shared.module';


@NgModule({
  declarations: [ProductComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    LayoutModule,
    SharedModule
  ]
})
export class ProductModule {
}
