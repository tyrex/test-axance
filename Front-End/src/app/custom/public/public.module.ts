import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PublicRoutingModule} from './public-routing.module';
import {PublicComponent} from './public.component';
import {LayoutModule} from '../layout/layout.module';


@NgModule({
  declarations: [PublicComponent],
  imports: [
    CommonModule,
    LayoutModule,
    PublicRoutingModule
  ]
})
export class PublicModule {
}
