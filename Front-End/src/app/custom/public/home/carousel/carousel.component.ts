import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  // ********************************* Carousels slides ***********************************//
  slides = [
    {category: 'Home Furmishings', image: 'assets/img/carousel/carousel-1.png', name: 'White Clock'},
    {category: 'Accessories', image: 'assets/img/carousel/carousel-1.png', name: 'White Clock'},
    {category: 'Sports', image: 'assets/img/carousel/carousel-1.png', name: 'White Clock'},
    {category: 'Clothing Wear', image: 'assets/img/carousel/carousel-1.png', name: 'White Clock'},
  ];

  // ------------------------------------------- End ----------------------------------------------

  SlideOptions = {
    items: 1, dots: true, nav: false
  };

  constructor() {
  }

  ngOnInit() {
  }

}
