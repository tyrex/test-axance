import {Component, OnInit} from '@angular/core';
import {ModalService} from '../../../../shared/services/modal/modal.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MODAL_MAP} from '../../../../shared/configurations/modalMap';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  newsLetterForm: FormGroup;

  constructor(private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    // ********************************* Building News letter form ***********************************//
    this.newsLetterForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.email],
    });
    // ------------------------------------------- End ----------------------------------------------
  }

  closeModal() {
    ModalService.hide('info');
    this.router.navigateByUrl('/public/home');
  }

  onSubmit() {
    ModalService.show(MODAL_MAP.INFO.type);
  }

}
