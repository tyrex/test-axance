import {Component, OnInit} from '@angular/core';
import {AnimationService} from '../../../../shared/services/animation-service/animation.service';

@Component({
  selector: 'app-indoors-outdoors',
  templateUrl: './indoors-outdoors.component.html',
  styleUrls: ['./indoors-outdoors.component.scss']
})
export class IndoorsOutdoorsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
    AnimationService.animation2('js-wp-1');
  }

}
