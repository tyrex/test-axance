import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndoorsOutdoorsComponent } from './indoors-outdoors.component';

describe('IndoorsOutdoorsComponent', () => {
  let component: IndoorsOutdoorsComponent;
  let fixture: ComponentFixture<IndoorsOutdoorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndoorsOutdoorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndoorsOutdoorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
