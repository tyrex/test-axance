import {Component, OnInit} from '@angular/core';
import {ProductService} from '../product/services/product.service';
import {Product} from '../../../shared/models/Product';
import {nextPage, previousPage} from '../../../core/utils/utility-functions';
import {Catalog} from '../../../shared/models/Catalog';
import {AnimationService} from '../../../shared/services/animation-service/animation.service';
import getAllCatalogs from "../../../shared/API/mocks/getAllCatalogs";
import getAllProducts from "../../../shared/API/mocks/getAllProducts";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Products variables
  products: Product[];
  productsToShow: Product[] = [];

  // Catalogs variables
  catalogs: Catalog[];
  catalogsToShow: Catalog[] = [];

  // Settings objects
  popSectionSettings = {index: 0, itemsPerPage: 8};
  collSectionSettings = {index: 0, itemsPerPage: 4};

  // Main Constructor
  constructor(private productService: ProductService) {
  }

  // Ng On Init
  ngOnInit() {
    // ************************************* Getting all Products *************************************//
    // Slice the products to the number fo items per page
    this.productService.getAllProducts().then((result: Product[]) => {
      this.products = result;
    }, (error) => {
      console.error(error);
      this.products = getAllProducts;
    }).finally(() => {
      this.productsToShow = this.products.slice(0, this.popSectionSettings.itemsPerPage);
    });

    // ************************************* Getting all catalogs *************************************//
    this.productService.getAllCatalogs().then((result: Catalog[]) => {
      this.catalogs = result;
    }, (error) => {
      this.catalogs = getAllCatalogs;
      console.error(error);
    }).finally(() => {
      this.catalogsToShow = this.catalogs.slice(0, this.collSectionSettings.itemsPerPage);
    });

    AnimationService.animation2('js-wp-2');
  }

  /**
   *
   *
   *
   *
   *
   */
  // ************************************* Handle paginations *************************************//
  // Manually
  goToPreviousPop() {
    this.productsToShow = previousPage(this.products, this.productsToShow, this.popSectionSettings);
  }// ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // *************************** Go to next page of popular products ******************************//
  goToNextPop() {
    this.productsToShow = nextPage(this.products, this.productsToShow, this.popSectionSettings);
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ***************************** Go to previous page of collection *****************************//
  goToNextColl() {
    this.catalogsToShow = nextPage(this.catalogs, this.catalogsToShow, this.collSectionSettings);
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ***************************** Go to previous page of collection ****************************//
  goToPreviousColl() {
    this.catalogsToShow = previousPage(this.catalogs, this.catalogsToShow, this.collSectionSettings);
  } // ------------------------------------------- End ----------------------------------------------
  /**
   *
   *
   *
   *
   *
   */
  // ******************************** trackBy function for loops ********************************//
  // Performance need
  trackByFn(index, item) {
    return item.id;
  }// ------------------------------------------- End ----------------------------------------------

}
