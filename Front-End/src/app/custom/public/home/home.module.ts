import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeRoutingModule} from './home-routing.module';
import {OwlModule} from 'ngx-owl-carousel';
import {SharedModule} from '../../../shared/shared.module';
import {HttpClientModule} from '@angular/common/http';
import {RequestService} from '../../../shared/services/request-service/request.service';
import {ProductService} from '../product/services/product.service';
import {ProductListComponent} from '../../layout/product-list/product-list.component';
import {LayoutModule} from '../../layout/layout.module';
import {ReactiveFormsModule} from '@angular/forms';
import { NewsletterComponent } from './newsletter/newsletter.component';
import { CarouselComponent } from './carousel/carousel.component';
import { IndoorsOutdoorsComponent } from './indoors-outdoors/indoors-outdoors.component';
import { CollectionsComponent } from './collections/collections.component';


@NgModule({
  declarations: [HomeComponent, NewsletterComponent, CarouselComponent, IndoorsOutdoorsComponent, CollectionsComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    LayoutModule,
    OwlModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class HomeModule {
}
