import {Component, Input, OnInit} from '@angular/core';
import {GlobalService} from '../../../../shared/services/global-service/global.service';
import {AnimationService} from '../../../../shared/services/animation-service/animation.service';


@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss']
})
export class CollectionsComponent implements OnInit {

  @Input() catalogsToShow;

  constructor() {
  }

  ngOnInit() {
    AnimationService.animation1('js-wp-3');
  }

  /**
   *
   *
   *
   *
   *
   */
  getRandomBgClass(index) {
    return GlobalService.getRandomBgClass(index);
  }

}
