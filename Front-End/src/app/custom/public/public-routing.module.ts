import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PublicComponent} from './public.component';

export const publicRoutes: Routes = [
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  //   // loadChildren: './home/home.module#HomeModule'
  // },
  // {
  //   path: 'products',
  //   loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
  //   // loadChildren: './product/product.module#ProductModule'
  // },
  {
    path: '',
    // redirectTo: 'home',
    component: PublicComponent,
    // pathMatch: 'full'
    children: [
      {
        path: 'home',
        // loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: 'products',
        // loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
        loadChildren: './product/product.module#ProductModule'
      },
      {
        path: '**',
        redirectTo: 'home',
      }
    ]
  }

] as Routes;

@NgModule({
  imports: [RouterModule.forChild(publicRoutes)],
  exports: [RouterModule]
})
export class PublicRoutingModule {
}
