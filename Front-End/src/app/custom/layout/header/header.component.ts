import {Component, OnInit} from '@angular/core';
// import * as $ from 'jquery';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

declare let $;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  searchForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      keyword: [''],
    });

    this.searchShow();

    $('.dropdown-toggle').dropdown();
  }
  /**
   *
   *
   *
   *
   *
   */
  searchShow() {
    // alert();
    const searchWrap = $('.search-wrap');
    $('.js-search-open').on('click', (e) => {
      e.preventDefault();
      searchWrap.addClass('active');
      setTimeout(() => {
        searchWrap.find('.form-control').focus();
      }, 300);
    });
    $('.js-search-close').on('click', (e) => {
      e.preventDefault();
      searchWrap.removeClass('active');
    });
  }
  /**
   *
   *
   *
   *
   *
   */
  // ************************************* Search form submit *************************************//
  onSubmit() {
    this.router.navigate(['/public/products'], {queryParams: {keyword: this.searchForm.controls.keyword.value}});
  }

}
