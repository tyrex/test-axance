import {Component, Input, OnInit} from '@angular/core';
import {Product} from '../../../shared/models/Product';
import {GlobalService} from '../../../shared/services/global-service/global.service';
import {AnimationService} from '../../../shared/services/animation-service/animation.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @Input() productsToShow: Product[];

  constructor() {
  }

  ngOnInit() {
    AnimationService.animation1('js-wp-2');
  }

  getRandomBgClass(index) {
    return GlobalService.getRandomBgClass(index);
  }

  // ******************************** trackBy function for loops ********************************//
  // Performance need
  trackByFn(index, item) {
    return item.id;
  }// ------------------------------------------- End ----------------------------------------------

}
