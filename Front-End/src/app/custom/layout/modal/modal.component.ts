import {Component, Input, NgZone, OnInit} from '@angular/core';
import {ModalService} from '../../../shared/services/modal/modal.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  @Input() type;
  @Input() content;
  @Input() title;
  @Input() onConfirm: () => {};
  @Input() onCancel: () => void;
  @Input() onClose: () => {};
  @Input() onRetry: () => {};
  @Input() id;
  @Input() onShow: () => {};
  @Input() cancelBtn;
  showContinue = false;

  constructor(private zone: NgZone, private router: Router) {
    console.log('init  modal ...', this.id);
    // Subscribe to modal event
    ModalService.modalEvent.subscribe(data => {
      this.showContinue = false;
      if (data.type === 'confirm') {
        this.onConfirm = data.callBack;
        this.onCancel = () => {
          ModalService.hide(data.id);
        };
      } else if (data.type === 'error') {
        this.onCancel = data.callBack;
      } else if (data.type === 'info') {
        this.showContinue = true;
        this.onConfirm = data.callBack;
      }
      this.zone.run(() => {
        this.content = data.content;
      });

    });


  }

  ngOnInit() {
  }

  closeModal() {
    if (!this.onClose) {
      ModalService.hide(this.id);
      ModalService.hideAll();
      // this.router.navigateByUrl('/public/home');
    } else {
      this.onClose();
    }
  }

}
