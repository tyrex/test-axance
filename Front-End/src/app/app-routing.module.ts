import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: 'public', pathMatch: 'full'},
  {
    path: 'public',
    // loadChildren: () => import('./custom/public/public.module').then(m => m.PublicModule)
    loadChildren: './custom/public/public.module#PublicModule'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false, scrollPositionRestoration: 'disabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
