import * as _underscore from 'lodash';

declare let navigator;
declare let DOMParser;
declare let ActiveXObject;
declare let _Url;
import * as moment from 'moment';

declare let window;
declare let document;

export function isEmpty(value) {
  return ((value === undefined) || (value === null) || (value === '') || (value === 'undefined'));
}

export function getDirection() {
  return localStorage.getItem('lang') === 'en' ? 'ltr' : 'rtl';
}

export function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

export function getLanguage() {
  return localStorage.getItem('lang');
}

/**
 * Makes a string's first letter uppercase.
 *
 * @return {string} Original string, but with first letter in upper case.
 */
export function upperCaseFirst(str) {
  return str.charAt(0).toUpperCase() + str.substring(1);
}

/**
 * Returns whether the page is in LTR mode. Defaults to `true` if it can't be computed.
 *
 * @return {boolean} Page's language direction is left-to-right.
 */
export function isLTR(): boolean {
  let dir = 'ltr';

  if (window) {
    if (window.getComputedStyle) {
      dir = window.getComputedStyle(document.body, null).getPropertyValue('direction');
    } else {
      dir = (document.body as any).currentStyle.direction;
    }
  }

  return dir === 'ltr';
}

export function isOnline(): boolean {
  return navigator.onLine;
}

export function isOffline(): boolean {
  return !navigator.onLine;
}

/**
 * Returns whether or not the current device is an iOS device.
 *
 * @return {boolean} Device is an iOS device (i.e. iPod touch/iPhone/iPad).
 */
export function isIOS(): boolean {
  return /iPad|iPhone|iPod/.test(navigator.userAgent) && !(window as any).MSStream;
  /*try{
   return cordova != undefined && cordova.platformId == "ios";
   }catch(ex){}
   return false;*/
}

/**
 * Returns whether or not this is running in a browser context.
 *
 * @return {boolean} Running in a browser context.
 */
export function isBrowser(): boolean {
  return typeof window !== 'undefined';
}

export function chunk(str, size: number) {
  return str.match(new RegExp('(.|[\r\n]){1, ${size} }', 'g'));
}

export function isNumeric(number: any) {
  // return (/^[+-]?([0-9]*[.])?[0-9]+$/.test(number);
  // return __isNumeric(number);
  return (/^([0-9]*[.])?[0-9]+$/.test(number));
}

export function validPhone(phone: any) {
  return (/^[\u0660-\u0669]{11}$/.test(phone));
}

export function isArabicNumeric(number: any) {
  return (/[\u0660-\u0669]/.test(number));
}


export function isNormalInteger(number: any) {
  return /^\+?(0|[1-9]\d*)$/.test(number);
}

export function scrollTop(selector?: string) {
  const _timeout: any = window.setTimeout(() => {
    window.scrollTo(0, 1);
    try {
      const element = document.getElementById(selector || 'main-content');
      if (element) {
        element.scrollTop = 0;
      }
    } catch (e) {
    }
    window.clearTimeout(_timeout);
  }, 0);
}

export function getValueObject(array: any[], searchKey, searchValue) {
  const critiria = {};
  let searchResult;
  if (!isEmpty(array) && !isEmpty(searchKey) && !isEmpty(searchValue)) {
    critiria[searchKey] = searchValue;
    searchResult = _underscore.find(array, critiria);
  }
  return searchResult;
}

export function getPropertyValue(_obj, searchKey) {

  return (_obj !== undefined && _obj[searchKey] !== undefined) ? _obj[searchKey] : undefined;
}

export function parseDrillDownValues(array: any[], displayKey, valueKey) {

  const temp: any = [];
  let map: any = {};
  _underscore.forEach(array, (item) => {

    if (item[displayKey] !== undefined && !map[item[displayKey]]) {
      temp.push({
        display: item[displayKey],
        value: item[valueKey],
      });
    }
    map[item[displayKey]] = true;
  });
  map = undefined;

  const result = _underscore.orderBy(temp, 'display', 'asc');
  return result;
}

export function removeItemFromArray(array: any[], key, value) {

  const criteria: any = {};
  criteria[key] = value;
  return _underscore.remove(array, criteria);
}

export function isPresent(obj) {
  return obj !== undefined && obj !== null;
}

export function isDate(obj) {
  return !/Invalid|NaN/.test(new Date(obj).toString());
}

export function formatDate(_date: any, pattern?: string): string {// Date or string
  const TIMESTAMP_REGEX = /^(\d{13})?$/;
  try {
    if (!isEmpty(_date)) {
      if (moment(_date).isValid()) {
        return moment(_date).format(pattern || 'YYYY-MM-DD');
      } else if (TIMESTAMP_REGEX.test(_date)) {
        return moment(parseInt(_date)).format(pattern || 'YYYY-MM-DD');
      }
    }
  } catch (e) {
  }
  return undefined;
}

export function createDate(_dateString?: any): Date {
  if ((typeof (_dateString) === 'string' && moment(_dateString).isValid())) {
    return new Date(_dateString);
  } else if ((_dateString instanceof Date)) {
    return _dateString;
  }
  return undefined;
}

export function escapeWhiteSpaces(data: any[], objectKeys: string[]) {
  const result: any = [];
  _underscore.forEach(data, (item) => {
    _underscore.forEach(objectKeys, (key) => {
      if (item[key] !== undefined) {
        item[key] = item[key].replace(/\s/g, '');
      }
    });
    result.push(item);
  });
  return result;
}

export function numberRound(value: number, places: number): number {

  const res = value;
  try {
    // let multiplier = Math.pow(10, places);
    // res = (Math.round(value * multiplier) / multiplier);
    places = places || 0;
    places = Math.pow(10, places);
    return Math.floor(value * places) / places;
  } catch (e) {
  }
  return res;
}

export function getMaxValue(_x: any, _y: any): number {
  return _x > _y ? _x : _y;
}

export function getMinValue(_x: any, _y: any): number {
  return _x < _y ? _x : _y;
}

export function arrayMove(array, from, to) {
  if (Math.abs(from - to) > 60) {
    array.splice(to, 0, array.splice(from, 1)[0]);
  } else {
    // works better when we are not moving things very far
    const target = array[from];
    const inc = (to - from) / Math.abs(to - from);
    let current = from;
    for (; current !== to; current += inc) {
      array[current] = array[current + inc];
    }
    array[to] = target;
  }
}

export function getBrowserName() {
  // Return cached result if avalible, else get result then cache it.
  if (getBrowserName.prototype._cachedResult) {
    return getBrowserName.prototype._cachedResult;
  }

  // Opera 8.0+
  const isOpera = (!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

  // Firefox 1.0+
  const isFirefox = typeof window.InstallTrigger !== 'undefined';

  // Safari 3.0+ "[object HTMLElementConstructor]"
  const isSafari = /constructor/i.test(window.HTMLElement) || (function(p) {
    return p.toString() === '[object SafariRemoteNotification]';
  })(!window.safari || window.safari.pushNotification);

  // Internet Explorer 6-11
  const isIE = /*@cc_on!@*/false || !!document.documentMode;

  // Edge 20+
  const isEdge = !isIE && !!window.StyleMedia;

  // Chrome 1+
  const isChrome = !!window.chrome && !!window.chrome.webstore;

  // Blink engine detection
  // let isBlink = (isChrome || isOpera) && !!window.CSS;

  return getBrowserName.prototype._cachedResult =
    isOpera ? 'Opera' :
      isFirefox ? 'Firefox' :
        isSafari ? 'Safari' :
          isChrome ? 'Chrome' :
            isIE ? 'IE' :
              isEdge ? 'Edge' :
                'Unknown  Browser';
}

export function getWebNavigatorIP(onNewIP) {
  //  onNewIp - your listener function for new IPs
  // compatibility for firefox and chrome

  const myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
  if (!myPeerConnection) {
    onNewIP('');
    return;
  }
  let pc = new myPeerConnection({
      iceServers: []
    }),
    noop = function() {
    },
    localIPs = {},
    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,
    key;

  function iterateIP(ip) {
    if (!localIPs[ip]) {
      onNewIP(ip);
    }
    localIPs[ip] = true;
  }

  // create a bogus data channel
  pc.createDataChannel('');

  // create offer and set local description
  pc.createOffer().then((sdp) => {
    //noinspection TypeScriptUnresolvedletiable
    sdp.sdp.split('\n').forEach((line) => {
      if (line.indexOf('candidate') < 0) {
        return;
      }
      line.match(ipRegex).forEach(iterateIP);
    });

    pc.setLocalDescription(sdp, noop, noop);
  }).catch((reason) => {
    // An error occurred, so handle the failure to connect
  });

  // listen for candidate events
  pc.onicecandidate = (ice) => {
    if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) {
      return;
    }
    ice.candidate.candidate.match(ipRegex).forEach(iterateIP);
  };
}

export function stringtoBase64(str: string) {
  try {
    const result = window.btoa(window.unescape(window.encodeURIComponent(str)));
    if (result !== undefined && result != null) {
      return result;
    }
  } catch (ex) {
  }
  return '';
}

export function formatUrl(url) {
  //  ^ - Only match at the beginning of the string
  //  http - Match the literal string "http"
  //  s? - Optionally match an "s"
  //  : - Match a colon
  //  \/\/ - Escape the "/" characters since they mark the beginning/end of the regular expression
  // The "i" after the regular expression makes it case-insensitive so it will match "HTTP://", etc.
  if (!/^https?:\/\//i.test(url)) {
    url = 'http://' + url;
  }
  return url;
}

export function getLastDayInMonth(monthIndex) {

  const date = new Date();
  date.setMonth(monthIndex - 1);
  return (new Date(date.getFullYear(), date
    .getMonth() + 1, 0)).getDate();
}

export function isLastDayInMonth(date: Date) {
  return (date.getDate() === (new Date(date.getFullYear(), date
    .getMonth() + 1, 0)).getDate());
}


export function parseXML(val: any) {
  try {
    let xmlDoc: any = {};
    if (document.implementation && document.implementation.createDocument) {
      xmlDoc = new DOMParser().parseFromString(val, 'text/xml');
    } else if (window.ActiveXObject) {
      xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
      xmlDoc.loadXML(val);
    } else {
      return null;
    }
    return xmlDoc;
  } catch (ex) {
    return null;
  }
}

export function xmlToJson(xml: any) {

  try {
    let obj: any = {};
    if (xml.nodeType === 1) {
      if (xml.attributes.length > 0) {
        obj['@attributes'] = {};
        for (let j = 0; j < xml.attributes.length; j++) {
          const attribute = xml.attributes.item(j);
          obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
        }
      }
    } else if (xml.nodeType === 3) {
      obj = xml.nodeValue;
    }
    if (xml.hasChildNodes()) {
      for (let i = 0; i < xml.childNodes.length; i++) {
        const item = xml.childNodes.item(i);
        const nodeName = item.nodeName;
        if (typeof (obj[nodeName]) === 'undefined') {
          obj[nodeName] = xmlToJson(item);
        } else {
          if (typeof (obj[nodeName].push) === 'undefined') {
            const old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(xmlToJson(item));
        }
      }
    }
    return obj;
  } catch (ex) {
    return null;
  }
}

export function getCurrentLang() {
  const lang = localStorage.getItem('lang');
  return lang ? lang : 'en';
}

/**
 * Use for convert Persian numbers to English numbers.
 * This function works on both Persian and Arabic numbers.
 *
 * @param {String} value
 * @return {string}
 **/
export function persianNumberToEng(value) {
  let newValue = '';
  try {
    for (let i = 0; i < value.length; i++) {
      const ch = value.charCodeAt(i);
      if (ch >= 1776 && ch <= 1785) {  // For Persian digits.{
        const newChar = ch - 1728;
        newValue = newValue + String.fromCharCode(newChar);
      } else if (ch >= 1632 && ch <= 1641) { // For Arabic digits.
        const newChar = ch - 1584;
        newValue = newValue + String.fromCharCode(newChar);
      } else {
        newValue = newValue + String.fromCharCode(ch);
      }

    }
  } catch (ex) {
    newValue = value;
  }
  return newValue;
}

// Sort collection items based on array of keys
export function sortObjectItemsByKeys(object, sortedKeys) {

  // Add sorted elements based on sorted keys
  const result = [];
  _underscore.forEach(sortedKeys, function(key) {
    if (object[key]) {
      result.push({
        key,
        value: object[key]
      });
    }
  });

  // Extract the difference between sorted keys and object keys
  const centrifuge = _underscore.spread(_underscore.difference);
  const unifiedKeys = centrifuge([
    Object.keys(object),
    sortedKeys
  ]).sort();

  // Add the rest of object elements
  _underscore.forEach(unifiedKeys, function(key) {
    if (object[unifiedKeys.indexOf(key)]) {
      result.push({
        key,
        value: object[unifiedKeys.indexOf(key)]
      });
    }
  });

  return result;
}

export function diff_minutes(dt2, dt1) {
  let diff = diff_seconds(dt2, dt1);
  diff /= 60;
  return Math.abs(Math.round(diff));
}

export function diff_seconds(dt2, dt1) {
  const diff = (dt2.getTime() - dt1.getTime()) / 1000;
  return Math.abs(Math.round(diff));
}

export function stepBack(step?: number) {
  if (step !== undefined) {
    history.go(step);
  } else {
    history.go(-1);
  }
}

/**
 * This function will handle the conversion from a file to base64 format
 *
 * @path string
 * @callback function receives as first parameter the content of the image
 */

export function memorySizeOf(obj) {
  let bytes = 0;

  function sizeOf(obj) {
    if (obj !== null && obj !== undefined) {
      switch (typeof obj) {
        case 'number':
          bytes += 8;
          break;
        case 'string':
          bytes += obj.length * 2;
          break;
        case 'boolean':
          bytes += 4;
          break;
        case 'object':
          const objClass = Object.prototype.toString.call(obj).slice(8, -1);
          if (objClass === 'Object' || objClass === 'Array') {
            for (const key in obj) {
              if (!obj.hasOwnProperty(key)) {
                continue;
              }
              sizeOf(obj[key]);
            }
          } else {
            bytes += obj.toString().length * 2;
          }
          break;
      }
    }
    return bytes;
  }

  function formatByteSize(bytes) {
    if (bytes < 1024) {
      return bytes + ' bytes';
    } else if (bytes < 1048576) {
      return (bytes / 1024).toFixed(3) + ' KiB';
    } else if (bytes < 1073741824) {
      return (bytes / 1048576).toFixed(3) + ' MiB';
    } else {
      return (bytes / 1073741824).toFixed(3) + ' GiB';
    }
  }

  return formatByteSize(sizeOf(obj));
}

export const nextPage = (fullList, listToShow, settings): [] => {
  if (((settings.index + 1) * settings.itemsPerPage) < fullList.length) {
    settings.index++;
    listToShow = fullList.slice(settings.index * settings.itemsPerPage,
      settings.index * settings.itemsPerPage + settings.itemsPerPage);
  }
  return listToShow;
};

export const previousPage = (fullList, listToShow, settings): [] => {
  if (settings.index > 0) {
    settings.index--;
    listToShow = fullList.slice(settings.index * settings.itemsPerPage,
      settings.index * settings.itemsPerPage + settings.itemsPerPage);
  }
  return listToShow;
};

export const getTotalPages = (listLength, itemsPerPage): number => {
  return Math.ceil(listLength / itemsPerPage) || 1;
};
