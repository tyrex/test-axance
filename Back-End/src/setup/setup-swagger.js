import swaggerUI from 'swagger-ui-express';

/**
 * Swagger specification for doc auto generation
 */
import swaggerSpecification from './setup-swagger-definitions';

export default (server) => {

    server.get('/swagger.json', (req, res) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(swaggerSpecification);
    });

    server.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecification));
}
