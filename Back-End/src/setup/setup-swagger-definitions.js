// const swaggerJSDoc = require('swagger-jsdoc');
import swaggerJSDoc from 'swagger-jsdoc';

const swaggerDefinition = {
    // openapi: '2.0.0', // Specification (optional, defaults to swagger: '2.0')
    info: {
        title: 'E-Commerce API', // Title (required)
        version: '1.0.0', // Version (required)
        description: 'Endpoints to test E-commerce APIs.'
    },
    basePath: '/',
    securityDefinitions: {
        bearerAuth: {
            type: 'apiKey',
            name: 'Authorization',
            scheme: 'bearer',
            in: 'header',
        },
    },
    // components: {
        // securitySchemes: {
        //     bearerAuth: {
        //         type: 'http',
        //         scheme: 'bearer',
        //         bearerFormat: 'JWT',
        //     }
        // }
    // },
    // security: [{
    //     bearerAuth: []
    // }]
};
const options = {
    swaggerDefinition,
    apis: [
        './src/*.js',
        './src/modules/*/controllers/*.js',
    ]
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
const swaggerSpecification = swaggerJSDoc(options);

module.exports = swaggerSpecification;
