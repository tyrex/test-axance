// Imports
import ip from 'ip'
// import date format
import format from 'date-fns/format'

// App Imports
import { PORT, NODE_ENV } from '../config/env'

// Start server
export default  (server) => {
    console.info('SETUP - Starting server..');

    /**
     * Starting server
     */
    const serverProcess = server.listen(PORT, (error) => {
        if (error) {
            console.error('ERROR - Unable to start server.');
        } else {
            console.info(`INFO  - Server started on...`);
            console.info(`  Local   http://localhost:${ PORT } [${ NODE_ENV }]`);
            console.info(`  Network http://${ ip.address() }:${ PORT } [${ NODE_ENV }]`);
            console.info(`  Datetime ${ format(new Date(), 'dd-MM-yyyy hh:mm:ss a')} \n`);
        }
    });

    // Stop Server
    for(let signal of ['SIGINT', 'SIGTERM']) {
        process.on(signal, function () {
            console.info('INFO - Shutting down server..');

            serverProcess.close(function () {
                console.info('INFO - Server has been shut down.');
            });
        });
    }
}
