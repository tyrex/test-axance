import {formatResponse} from "../common/helpers/formatter";
// Response interceptor
// import {mung} from "express-mung";
const mung = require('express-mung');

export default (server) => {
    //middleware to access response body object
    server.use(mung.json(
        function transform(body, req, res) {
            console.error('error catched -------------->', req.errorCatched);
            // do something with body
            if (!req.errorCatched) {
                body = formatResponse(body, null);
            }
            return body;
        }
    ));
}
