// Imports
import express from 'express'
import path from 'path'
import helmet from 'helmet';
import cors from 'cors';
import compression from 'compression';
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser';
import morgan from 'morgan';

// App Imports
import {NODE_ENV} from '../config/env'

export default (server) => {
    console.info('SETUP - Loading middlewares...');

    // XSS protections , noSniff, HTTP Strict Transport Security , prevent clickjacking, setting Content Security Policy
    server.use(helmet());

    // enable CORS with various options.
    server.use(cors());

    //Gzip compressing can greatly decrease the size of the response body
    server.use(compression());

    // Parse incoming request bodies in a middleware before your handlers, available under the req.body property.
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({extended: false}));

    // Request body cookie parser
    server.use(cookieParser());

    // Static files folder
    server.use('/resources', express.static(path.join(__dirname, '..', '..', 'public')));

    //HTTP request logger middleware for node.js
    // HTTP logger
    if (NODE_ENV === 'development') {
        server.use(morgan('tiny'))
    }
}
