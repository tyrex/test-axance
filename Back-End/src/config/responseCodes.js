export const responseCodes = {
    SUCCESS: {code: '200', message: 'success'},
    SERVER_ERROR: {code: '500', message: 'Server error'},
    REQUEST_VALIDATION: {code: '403', message: 'Verify your request'},
    INVALID_TOKEN: {code: '401', message: 'Unauthorized'},
    INVALID_USER: {code: '301', message: 'Invalid User'},
};
