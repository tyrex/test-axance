// Imports
import dotenv from 'dotenv'

// Load .env
dotenv.config();

// Environment
export const NODE_ENV = process.env.NODE_ENV || 'development';

// Security
export const SECURITY_SECRET = process.env.SECURITY_SECRET || 'sameh123';
export const SECURITY_SALT_ROUNDS = parseInt(process.env.SECURITY_SALT_ROUNDS);

// Port
export const PORT = process.env.PORT || 3000;

// Database
export const DB_NAME = 'node-ecommerce';

// URL
export const LANDING_URL = process.env.LANDING_URL;
export const WEB_URL = process.env.WEB_URL;
export const API_URL = process.env.API_URL;

// Email
export const EMAIL_ON = process.env.EMAIL_ON;
export const EMAIL_TEST = process.env.EMAIL_TEST;
export const EMAIL_HOST = process.env.EMAIL_HOST;
export const EMAIL_USER = process.env.EMAIL_USER;
export const EMAIL_PASSWORD = process.env.EMAIL_PASSWORD;

// Global parameters
export const REQUEST_EVENT_LOGGER = true;
export const REQUEST_EVENT_EMAILER = false;
export const EVENT_LOGGER_NAME = 'REQUEST_LOGGER_EVENT';

// Redis parameters
export const REDIS_HOST = process.env.REDIS_HOST || '127.0.0.1';
export const REDIS_PORT = process.env.REDIS_PORT || 6379;


export const saltRounds = 10;
