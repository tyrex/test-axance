// const Joi = require('joi');
import Joi from 'joi';

const CREATE_PRODUCT = Joi.object().keys({
    userName: Joi.string().required().min(3),
    firstName: Joi.string().required().min(3),
    lastName: Joi.string().required().min(3),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
    repeatPassword: Joi.ref('password'),
});

module.exports = {CREATE_PRODUCT};
