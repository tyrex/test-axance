import models from "../../../sequelize/models";

/**
 * Centralized Default clauses
 * that will be used in all products requests
 * @type {{include: {model: *, attributes: {exclude: string[]}}[], attributes: {exclude: string[]}}}
 */
const defaultClauses = {
    attributes: {exclude: ['CategoryId', 'CatalogId', 'createdAt', 'updatedAt']},
    include: [{
        model: models.Category,
        attributes: {exclude: ['createdAt', 'updatedAt']},
    }, {
        model: models.Catalog,
        attributes: {exclude: ['createdAt', 'updatedAt']},
    }]
};

/**
 * Get products by keyword param passed from request
 * @param keyword
 * @returns {Promise<Model<any, any>[]>}
 */
export function getProductsBySearch(keyword) {
    return models.Product.findAll({
        where: {
            [models.Sequelize.Op.or]: [
                {productName: {[models.Sequelize.Op.like]: `%${keyword}%`}},
                {productDescription: {[models.Sequelize.Op.like]: `%${keyword}%`}}
            ]
        },
        ...defaultClauses
    });
}

/**
 * Get all products including categories
 * @returns {Promise<Model<any, any>[]>}
 */
export function getAllProducts() {
    return models.Product.findAll({...defaultClauses});
}

/**
 * Get products by category ID
 * @param categID
 * @returns {Promise<Model<any, any>[]>}
 */
export function getAllProductsByCategory(categID) {
    return models.Product.findAll({
        where: {
            CategoryId: categID
        },
        ...defaultClauses
    });
}

/**
 * Get products By catalog ID
 * @param catalogID
 * @returns {Promise<Model<any, any>[]>}
 */
export function getAllProductsByCatalog(catalogID) {
    return models.Product.findAll({
        where: {
            CatalogId: catalogID
        },
        ...defaultClauses
    });
}

/**
 * Get all catalogs including
 * @returns {Promise<Model<any, any>[]>}
 */
export function getAllCatalogs() {
    return models.Catalog.findAll({attributes: {exclude: ['createdAt', 'updatedAt']}});
}
