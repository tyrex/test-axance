// --------------------------------- IMPORTS -------------------------------
import {
    getAllCatalogs,
    getAllProducts,
    getAllProductsByCatalog,
    getAllProductsByCategory,
    getProductsBySearch
} from "./products.repository";

// ------------------------------ Get products -----------------------------
const getProducts = async (req, res, next) => {
    console.log('params ------------->', req.query);
    let products;
    try {
        if (req.query.search) {
            products = await getProductsBySearch(req.query.search);
        } else {
            products = await getAllProducts();
        }
        // Return response to client
        res.json(products);
    } catch (error) {
        console.error('error ---------->', error);
        next(error);
    }
};

// ------------------------------ Get products By Category -----------------------------
const getProductsByCategory = async (req, res, next) => {
    let products;
    try {
        if (req.query.categID) {
            products = await getAllProductsByCategory(req.query.categID);
        } else {
            products = await getAllProducts();
        }
        // Return response to client
        res.json(products);
    } catch (error) {
        console.error('error ---------->', error);
        next(error);
    }
};

// ------------------------------ Get products By Category -----------------------------
const getProductsByCatalog = async (req, res, next) => {
    let products;
    try {
        if (req.query.catalogID) {
            products = await getAllProductsByCatalog(req.query.catalogID);
        } else {
            products = await getAllProducts();
        }
        // Return response to client
        res.json(products);
    } catch (error) {
        console.error('error ---------->', error);
        next(error);
    }
};


// ------------------------------ Get catalogs -----------------------------
const getCatalogs = async (req, res, next) => {
    let catalogs;
    try {
        catalogs = await getAllCatalogs();
        // Return response to client
        res.json(catalogs);
    } catch (error) {
        console.error('error ---------->', error);
        next(error);
    }
};
module.exports = {getProducts, getProductsByCategory, getProductsByCatalog, getCatalogs};

