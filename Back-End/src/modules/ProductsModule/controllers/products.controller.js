const Router = require('express').Router();
import productService from '../services/products.service';

/**
 * @swagger
 * /products/getAllProducts:
 *   get:
 *     tags:
 *       - products
 *     name: get all products
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: search
 *         in: query
 *         type: string
 *     responses:
 *       '200':
 *         description: Products found
 *         schema:
 *           type: object
 *       '500':
 *         description: Product was not found
 */
Router.get('/getAllProducts', (req, res, next) => productService.getProducts(req, res, next));

/**
 * @swagger
 * /products/getAllProducts:
 *   get:
 *     tags:
 *       - products
 *     name: get all products
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: search
 *         in: query
 *         type: string
 *     responses:
 *       '200':
 *         description: Products found
 *         schema:
 *           type: object
 *       '500':
 *         description: Products not found
 */
Router.get('/getAllProducts', (req, res, next) => productService.getProducts(req, res, next));


/**
 * @swagger
 * /products/getAllProductsByCategory:
 *   get:
 *     tags:
 *       - products
 *     name: get all products By Category
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: categID
 *         in: query
 *         type: string
 *         required: true
 *     responses:
 *       '200':
 *         description: Products found
 *         schema:
 *           type: object
 *       '500':
 *         description: Products not found
 */
Router.get('/getAllProductsByCategory', (req, res, next) => productService.getProductsByCategory(req, res, next));


/**
 * @swagger
 * /products/getAllProductsByCatalog:
 *   get:
 *     tags:
 *       - products
 *     name: get all products By Catalog
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: catalogID
 *         in: query
 *         type: string
 *         required: true
 *     responses:
 *       '200':
 *         description: Products found
 *         schema:
 *           type: object
 *       '500':
 *         description: Products not found
 */
Router.get('/getAllProductsByCatalog', (req, res, next) => productService.getProductsByCatalog(req, res, next));


/**
 * @swagger
 * /products/getAllCatalogs:
 *   get:
 *     tags:
 *       - catalogs
 *     name: get all catalogs
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Catalogs found
 *         schema:
 *           type: object
 *       '500':
 *         description: Catlog was not found
 */
Router.get('/getAllCatalogs', (req, res, next) => productService.getCatalogs(req, res, next));


/*return Router;*/
module.exports = Router;
