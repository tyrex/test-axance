const Router = require('express').Router();
import userService from '../services/users.service';

/**
 * @swagger
 * /users/addUser:
 *   post:
 *     tags:
 *       - users
 *     name: add single user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: User object
 *         in:  body
 *     responses:
 *       '200':
 *         description: Add single user
 *         schema:
 *           type: object
 *       '500':
 *         description: User was not added
 */
Router.post('/addUser',  (req, res, next) => userService.create(req, res, next));


/**
 * @swagger
 *
 * definitions:
 *   User:
 *     type: object
 *     required:
 *       - userName
 *       - password
 *     properties:
 *       userName:
 *         type: string
 *       password:
 *         type: string
 *         format: password
 */

/**
 * @swagger
 * /users/authenticate:
 *   post:
 *     tags:
 *       - users
 *     name: authenticate user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: user
 *         description: User object
 *         in:  body
 *         required: true
 *         type: object
 *         schema:
 *           $ref: '#/definitions/User'
 *     responses:
 *       '200':
 *         description: User authenticated
 *         schema:
 *           type: object
 *       '500':
 *         description: User was not found
 */
Router.post('/authenticate', (req, res, next) => userService.authenticate(req, res, next));

/*return Router;*/
module.exports = Router;
