// --------------------------- IMPORTS -------------------------------
import Joi from 'joi';
import UserValidation from '../validators/users.validator';
import models from '../../../sequelize/models'
import {handleJoiAsyncValidation} from "../../../common/helpers/handlers";
import bcrypt from 'bcryptjs';
import {SECURITY_SECRET} from "../../../config/env";
import {responseCodes} from "../../../config/responseCodes";
import jwt from '../../../common/helpers/setup-jwt';

const {ErrorHandler} = require('../../../common/error-handling/error');

// ------------------------------ Create User --------------------------
const create = async (req, res, next) => {
    const joiValidation = await handleJoiAsyncValidation(Joi.validate(req.body, UserValidation.CREATE_USER), next);
    console.log('joiValidation', joiValidation);
    if (joiValidation) {
        console.log('hello');
    }
    // Joi.validate(req.body, UserValidation.CREATE_USER)
    //     .then((value) => {
    //         models.User.create(value).then(result => {
    //
    //         });
    //     }, (error) => {
    //         throw new ErrorHandler(responseCodes.SERVER_ERROR.code, error.details);
    //     }).catch((error) => {
    //     next(error);
    // });

};

// ------------------------------ Authenticate User --------------------------
const authenticate = async (req, res, next) => {
    try {
        const user = await models.User.findOne({where: {userName: req.body.userName}});

        // User not found
        if (!user) {
            // User does not exists
            throw new ErrorHandler(responseCodes.INVALID_USER.code, `We do not have any user registered with ${req.body.userName}. Please signup.`)
        }
        // User with userName found
        else {
            // Getting user details
            const userDetails = user.get();
            const {password, ...userWithoutPassword} = userDetails;

            // User exists
            const passwordMatch = await bcrypt.compareSync(req.body.password, userDetails.password);

            if (!passwordMatch) {
                // Incorrect password
                throw new ErrorHandler(responseCodes.INVALID_USER.code, `Sorry, the password you entered is incorrect. Please try again.`)
            } else {
                const userDetailsToken = {
                    id: userDetails.id,
                    userName: userDetails.userName,
                    email: userDetails.email,
                    jti: userDetails.userName,
                };

                res.json({
                    user: userWithoutPassword,
                    token: await jwt.sign(userDetailsToken, SECURITY_SECRET)
                });
            }
        }
    } catch (error) {
        console.error('error ---------->', error);
        next(error);
    }
};

module.exports = {create, authenticate};

