import {REQUEST_EVENT_LOGGER, EVENT_LOGGER_NAME, REQUEST_EVENT_EMAILER} from "../config/env";
import models from '../sequelize/models'
import events from 'events';
import nodemailer from 'nodemailer';

let requestEventEmitter = new events.EventEmitter();

if (REQUEST_EVENT_LOGGER) {
    requestEventEmitter.on(EVENT_LOGGER_NAME, (req, requestID, params) => {
        let request = {
            requestID: requestID,
            path: req.originalUrl,
            code: params.statusCode,
            message: JSON.stringify(params.message),
            requestDate: new Date(),
        };
        models.Request.create(request).then(result => {
            console.log('Request saved');
        });

        if (REQUEST_EVENT_EMAILER) {
            sendEmail(req, requestID, params);
        }

    });
}

function sendEmail(req, requestID, params) {
    let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'ghandrisemh@gmail.com',
            pass: 'mvmhfhkswhfzvfua'
        }
    });

    const mailOptions = {
        from: 'ghandrisemh@gmail.com', // sender address
        to: 'ghandrisemh@gmail.com', // list of receivers
        subject: 'Subject of your email', // Subject line
        html: '<p>Your html here</p>' + params.statusCode // plain text body
    };

    transporter.sendMail(mailOptions, function (err, info) {
        if (err)
            console.log(err);
        else
            console.log(info);
    });
}


module.exports = requestEventEmitter;

