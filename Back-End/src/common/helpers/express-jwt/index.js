import jwt from '../setup-jwt';
import {responseCodes} from "../../../config/responseCodes";

let UnauthorizedError = require('./errors/UnauthorizedError');
let unless = require('express-unless');
let async = require('async');
let set = require('lodash.set');
const {ErrorHandler} = require('../../error-handling/error');


let DEFAULT_REVOKED_FUNCTION = function (_, __, cb) {
    return cb(null, false);
};

function isFunction(object) {
    return Object.prototype.toString.call(object) === '[object Function]';
}

function wrapStaticSecretInCallback(secret) {
    return function (_, __, cb) {
        return cb(null, secret);
    };
}

module.exports = function (options) {
    if (!options || !options.secret) throw new Error('secret should be set');

    let secretCallback = options.secret;

    if (!isFunction(secretCallback)) {
        secretCallback = wrapStaticSecretInCallback(secretCallback);
    }

    let isRevokedCallback = options.isRevoked || DEFAULT_REVOKED_FUNCTION;

    let _requestProperty = options.userProperty || options.requestProperty || 'user';
    let _resultProperty = options.resultProperty;
    let credentialsRequired = typeof options.credentialsRequired === 'undefined' ? true : options.credentialsRequired;

    let middleware = function (req, res, next) {
        let token;

        if (req.method === 'OPTIONS' && req.headers.hasOwnProperty('access-control-request-headers')) {
            let hasAuthInAccessControl = !!~req.headers['access-control-request-headers']
                .split(',').map(function (header) {
                    return header.trim();
                }).indexOf('authorization');

            if (hasAuthInAccessControl) {
                return next();
            }
        }

        if (options.getToken && typeof options.getToken === 'function') {
            try {
                token = options.getToken(req);
            } catch (e) {
                return next(e);
            }
        } else if (req.headers && req.headers.authorization) {
            let parts = req.headers.authorization.split(' ');
            if (parts.length == 2) {
                let scheme = parts[0];
                let credentials = parts[1];

                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                } else {
                    if (credentialsRequired) {
                        // return next(new UnauthorizedError('credentials_bad_scheme', {message: 'Format is Authorization: Bearer [token]'}));
                        return next(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'Format is Authorization: Bearer [token]'));
                    } else {
                        return next();
                    }
                }
            } else {
                return next(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'Format is Authorization: Bearer [token]'));
            }
        }

        if (!token) {
            if (credentialsRequired) {
                return next(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'No authorization token was found'));
            } else {
                return next();
            }
        }

        let dtoken;

        try {
            dtoken = jwt.decode(token, {complete: true}) || {};
        } catch (err) {
            return next(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'invalid_token'));
        }

        async.waterfall([
            function getSecret(callback) {
                let arity = secretCallback.length;
                if (arity == 4) {
                    secretCallback(req, dtoken.header, dtoken.payload, callback);
                } else { // arity == 3
                    secretCallback(req, dtoken.payload, callback);
                }
            },
            function verifyToken(secret, callback) {
                jwt.verify(token, secret, options).then(decoded => {
                    callback(null, decoded);
                }, err => {
                    console.log('has error ------------>', err);
                    callback(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'invalid_token'));
                }).catch(error => {
                    console.log('token catch error ------------>', error);
                    callback(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'invalid_token'));
                })
                // jwt.verify(token, secret, options, function (err, decoded) {
                //     console.log('has token ------------>', err);
                //     if (err) {
                //         callback(new UnauthorizedError('invalid_token', err));
                //     } else {
                //         callback(null, decoded);
                //     }
                // });
            },
            function checkRevoked(decoded, callback) {
                isRevokedCallback(req, dtoken.payload, function (err, revoked) {
                    if (err) {
                        callback(err);
                    } else if (revoked) {
                        callback(new ErrorHandler(responseCodes.INVALID_TOKEN.code, 'The token has been revoked.'));
                    } else {
                        callback(null, decoded);
                    }
                });
            }

        ], function (err, result) {
            if (err) {
                return next(err);
            }
            if (_resultProperty) {
                set(res, _resultProperty, result);
            } else {
                set(req, _requestProperty, result);
            }
            next();
        });
    };

    middleware.unless = unless;
    middleware.UnauthorizedError = UnauthorizedError;

    return middleware;
};

module.exports.UnauthorizedError = UnauthorizedError;
