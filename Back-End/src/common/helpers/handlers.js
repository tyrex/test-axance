import {responseCodes} from "../../config/responseCodes";

const {handleError, ErrorHandler} = require('../error-handling/error');

export function handleJoiAsyncValidation(promiseFunction, next) {
    return new Promise((resolve, reject) => {
        promiseFunction
            .then((value) => {
                    resolve(value);
                },
                (error) => {
                    if (error.isJoi) {
                        throw new ErrorHandler(responseCodes.REQUEST_VALIDATION.code, error.details);
                    }
                })
            .catch((error) => {
                next(error);
            })
    });

}
