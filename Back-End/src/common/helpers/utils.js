import format from 'date-fns/format'
import uuid from 'uuid';

// const ErrorHandler = require('../error-handling/error');


export function formatResponse(req, err) {
    if (err) {
        const {statusCode, message} = err;
        return {
            messageStatus: {
                returnCode: statusCode,
                returnCodeDesc: message
            },
            messageBody: null,
            messageHeader: {
                requestId: uuid(),
                requestChannel: 'WEB',
                requestTime: format(new Date(), 'dd-MM-yyyy hh:mm:ss a')
            }
        }
    } else {
        return {
            messageStatus: {
                returnCode: '000',
                returnCodeDesc: 'No errors'
            },
            messageBody: req.body,
            messageHeader: {
                requestId: uuid(),
                requestChannel: 'WEB',
                requestTime: format(new Date(), 'dd-MM-yyyy hh:mm:ss a')
            }
        }
    }
}
