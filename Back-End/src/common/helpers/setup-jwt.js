import redis from 'redis';
import {REDIS_HOST, REDIS_PORT} from "../../config/env";

const JWTR = require('jwt-redis').default;
//ES6 import JWTR from 'jwt-redis';
const redisClient = redis.createClient({host: REDIS_HOST, port: REDIS_PORT});
const jwt = new JWTR(redisClient);

module.exports = jwt;
