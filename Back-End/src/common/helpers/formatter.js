import format from 'date-fns/format'
import uuid from 'uuid';
import {EVENT_LOGGER_NAME, REQUEST_EVENT_LOGGER} from "../../config/env";
import requestEventEmitter from '../event-subscriber'

export function formatResponse(req, err) {
    let requestID = uuid();
    if (err) {
        req.errorCatched = true;
        const {statusCode, message} = err;
        if (REQUEST_EVENT_LOGGER) {
            requestEventEmitter.emit(EVENT_LOGGER_NAME, req, requestID, err);
        }
        return {
            messageStatus: {
                returnCode: statusCode || 100,
                returnCodeDesc: message
            },
            messageBody: null,
            messageHeader: {
                requestId: requestID,
                requestChannel: 'WEB',
                requestTime: format(new Date(), 'dd-MM-yyyy hh:mm:ss a')
            }
        }
    } else {
        return {
            messageStatus: {
                returnCode: '000',
                returnCodeDesc: 'No errors'
            },
            messageBody: req.body || req,
            messageHeader: {
                requestId: requestID,
                requestChannel: 'WEB',
                requestTime: format(new Date(), 'dd-MM-yyyy hh:mm:ss a')
            }
        }
    }
}
