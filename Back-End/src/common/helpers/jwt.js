// import expressJwt from 'express-jwt';
import expressJwt from './express-jwt'
import {SECURITY_SECRET as secret} from "../../config/env";

function jwt() {
    try {
        return expressJwt({secret}).unless({
            path: [
                // public routes that don't require authentication
                '/users/authenticate',
                '/products/getAllProducts',
                '/products/getAllProductsByCatalog',
                '/products/getAllProductsByCategory',
                '/products/getAllCatalogs',
                '/v1'
                // '/error',
            ]
        });
    } catch (error) {
    }

}

module.exports = jwt;
