import {formatResponse} from "../helpers/formatter";
import {responseCodes} from "../../config/responseCodes";

class ErrorHandler extends Error {
    constructor(statusCode, message) {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }
}

const handleError = (err, req, res) => {
    console.log('request -------------->', req.body);
    res.status(responseCodes.SUCCESS.code).json(formatResponse(req, err));
};


module.exports = {
    ErrorHandler,
    handleError
};
