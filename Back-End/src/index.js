// Imports
import express from 'express';
// Load handling error
// import handleError from './common/error-handling/error';
const {handleError, ErrorHandler} = require('./common/error-handling/error');

// Load setups
import setupStartServer from './setup/start-server';
import loadSetupMiddlewares from './setup/load-middlewares';
import setupSwagger from './setup/setup-swagger';
import setupResponseFormatter from './setup/response-formatter';

// JWT helper
import jwt from './common/helpers/jwt';

// Load Event subscribers
require('./common/event-subscriber');

// Create express server
const server = express();

// Setup Middlewares for security and request parsing
loadSetupMiddlewares(server);

// Setup Swagger
setupSwagger(server);

// Start server
setupStartServer(server);

// Setup server response formatter
setupResponseFormatter(server);

// use JWT auth to secure the api
server.use(jwt());

/**
 * @swagger
 * /v1:
 *   get:
 *     tags:
 *       - /v1
 *     name: First API in the application
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     responses:
 *       '200':
 *         description: V1 of the API
 *       '404':
 *         description: File with given ID was not found
 */
server.get('/v1', (req, res) => {
    res.send('E-Commerce@V1.0.0');
});

/**
 * @swagger
 * /error:
 *   get:
 *     tags:
 *       - error
 *     name: Error handler
 *     security:
 *       - bearerAuth: []
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Error handler
 */
server.get('/error', (req, res) => {
    throw new ErrorHandler(500, 'Internal server error');
});

server.use('/users', require('./modules/UsersModule/controllers/users.controller'));
server.use('/products', require('./modules/ProductsModule/controllers/products.controller'));

server.use((err, req, res, next) => {
    handleError(err, req, res);
});

