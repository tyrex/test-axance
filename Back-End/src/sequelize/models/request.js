'use strict';
module.exports = (sequelize, DataTypes) => {
    const Request = sequelize.define('Request', {
        requestID: DataTypes.STRING,
        path: DataTypes.STRING,
        code: DataTypes.STRING,
        message: DataTypes.STRING,
        requestDate: DataTypes.DATE,
    }, {});
    Request.associate = function (models) {
        // associations can be defined here
    };
    return Request;
};
