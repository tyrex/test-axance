'use strict';
module.exports = (sequelize, DataTypes) => {
    const Catalog = sequelize.define('Catalog', {
        name: DataTypes.STRING,
    }, {});
    Catalog.associate = function (models) {
        Catalog.hasMany(models.Product);
    };
    return Catalog;
};
