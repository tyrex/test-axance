'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    productName: DataTypes.STRING,
    price: DataTypes.STRING,
    author: DataTypes.STRING,
    img: DataTypes.STRING,
    productDescription: DataTypes.TEXT,
  }, {});
  Product.associate = function(models) {
    // associations can be defined here
    Product.belongsTo(models.Category);
    Product.belongsTo(models.Catalog);
  };
  return Product;
};
