'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Requests', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            requestID: {
                type: Sequelize.STRING
            },
            path: {
                type: Sequelize.STRING
            },
            code: {
                type: Sequelize.STRING
            },
            message: {
                type: Sequelize.STRING
            },
            requestDate: {
                type: Sequelize.DATE
            },
            createdAt: {
                allowNull: true,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: true,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Requests');
    }
};
