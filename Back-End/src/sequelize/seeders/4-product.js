'use strict';

import faker from 'faker';

module.exports = {
    up: (queryInterface, Sequelize) => {
        let products = [];
        for (let i = 1; i < 20; i++) {
            products.push({
                id: i,
                productName: faker.commerce.productName(),
                price: faker.commerce.price(),
                productDescription: faker.lorem.paragraph(),
                author: 'John Doelson',
                img: faker.random.arrayElement(['products/product1.png', 'products/product2.png', 'products/product3.png', 'products/product4.png', 'products/product5.png', 'products/product6.png']),
                categoryId: faker.random.arrayElement([1, 2]),
                catalogId: faker.random.arrayElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
                createdAt: new Date(),
                updatedAt: new Date()
            })
        }
        return queryInterface.bulkInsert('Products', products)
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Products', null, {});
    }
};
