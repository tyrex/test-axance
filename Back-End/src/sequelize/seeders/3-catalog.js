'use strict';

import faker from 'faker';

module.exports = {
    up: (queryInterface, Sequelize) => {
        let catalogs = [];
        for (let i = 1; i < 10; i++) {
            catalogs.push({
                id: i,
                name: faker.commerce.department(),
            });
        }
        return queryInterface.bulkInsert('Catalogs', catalogs)
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Catalogs', null, {});
    }
};
