'use strict';

import faker from 'faker';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Categories', [
            {
                id: 1,
                name: faker.random.arrayElement(['OUTDOORS', 'INDOORS'])
            },
            {
                id: 2,
                name: faker.random.arrayElement(['OUTDOORS', 'INDOORS'])
            }
        ])
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Categories', null, {});
    }
};
