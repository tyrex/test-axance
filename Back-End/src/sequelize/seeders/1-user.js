'use strict';

import {saltRounds} from "../../config/env";
import faker from 'faker';
import bcrypt from 'bcryptjs';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Users', [
            {
                id: 1,
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                userName: faker.internet.userName(),
                email: faker.internet.email(),
                password: bcrypt.hashSync('123456', saltRounds),
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 2,
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                userName: faker.internet.userName(),
                email: faker.internet.email(),
                password: bcrypt.hashSync('123456', saltRounds),
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 3,
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                userName: faker.internet.userName(),
                email: faker.internet.email(),
                password: bcrypt.hashSync('123456', saltRounds),
                createdAt: new Date(),
                updatedAt: new Date()
            },
        ])
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
