# NodeJs SEQUELIZE

`Cannot use import statement outside a module` Error from ES6 import statements, needs babel devDependencies

## Ops
const Op = Sequelize.Op

`[Op.and]: [{a: 5}, {b: 6}]` // (a = 5) AND (b = 6) <br>
`[Op.or]: [{a: 5}, {a: 6}]`  // (a = 5 OR a = 6) <br>
`[Op.gt]: 6,         `       // > 6 <br>
`[Op.gte]: 6,     `          // >= 6 <br>
`[Op.lt]: 10,    `           // < 10 <br>
`[Op.lte]: 10,     `         // <= 10 <br>
`[Op.ne]: 20,      `         // != 20 <br>
`[Op.eq]: 3,       `         // = 3 <br>
`[Op.is]: null      `        // IS NULL <br>
`[Op.not]: true,     `       // IS NOT TRUE <br>
`[Op.between]: [6, 10],  `   // BETWEEN 6 AND 10 <br>
`[Op.notBetween]: [11, 15],` // NOT BETWEEN 11 AND 15 <br>
`[Op.in]: [1, 2],       `    // IN [1, 2] <br>
`[Op.notIn]: [1, 2],   `     // NOT IN [1, 2] <br>
`[Op.like]: '%hat',    `     // LIKE '%hat' <br>
`[Op.notLike]: '%hat'  `     // NOT LIKE '%hat' <br>
`[Op.iLike]: '%hat'      `   // ILIKE '%hat' (case insensitive) (PG only) <br>
`[Op.notILike]: '%hat'    `  // NOT ILIKE '%hat'  (PG only) <br>
`[Op.startsWith]: 'hat'   `  // LIKE 'hat%' <br>
`[Op.endsWith]: 'hat'     `  // LIKE '%hat' <br> 
`[Op.substring]: 'hat'   `   // LIKE '%hat%' <br>
`[Op.regexp]: '^[h|a|t]'  `  // REGEXP/~ '^[h|a|t]' (MySQL/PG only) <br>
`[Op.notRegexp]: '^[h|a|t]'` // NOT REGEXP/!~ '^[h|a|t]' (MySQL/PG only) <br>
`[Op.iRegexp]: '^[h|a|t]'   ` // ~* '^[h|a|t]' (PG only) <br>
`[Op.notIRegexp]: '^[h|a|t]'` // !~* '^[h|a|t]' (PG only) <br>
`[Op.like]: { [Op.any]: ['cat', 'hat']}`
                           // LIKE ANY ARRAY['cat', 'hat'] - also works for iLike and notLike <br>
`[Op.overlap]: [1, 2]  `     // && [1, 2] (PG array overlap operator) <br>
`[Op.contains]: [1, 2]  `    // @> [1, 2] (PG array contains operator) <br>
`[Op.contained]: [1, 2] `    // <@ [1, 2] (PG array contained by operator) <br>
`[Op.any]: [2,3]        `    // ANY ARRAY[2, 3]::INTEGER (PG only) <br>

`[Op.col]: 'user.organization_id' `// = "user"."organization_id", with dialect specific column identifiers, PG in this example <br>
`[Op.gt]: { [Op.all]: literal('SELECT 1') }`
                          // > ALL (SELECT 1) <br>
