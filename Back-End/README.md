# NodeJs E-commerce

## Sequelize

Run to create database `npx sequelize-cli db:create` <br>
Run to generate model `npx sequelize-cli model:generate --name User --attributes firstName:string,lastName:string,email:string`<br>
Run to create tables `npx sequelize-cli db:migrate` <br>
Run to seed data `npx sequelize-cli db:seed:all`
